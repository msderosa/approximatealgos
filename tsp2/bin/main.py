
import sys
import os
import traceback


def main():
    try:
        print("starting ...")

        get_base_directory()
        reader = R.Kgl()
        flds = reader.read()

        reader.write(flds)
        print("success")
    except Exception:
        traceback.print_exc()


def get_base_directory():
    d = os.getcwd()
    if not d.endswith('tsp2'):
        msg = 'program must be run from the root of the tsp2 directory'
        raise Exception(msg)
    return d


if __name__ == "__main__":
    signs_dir = os.path.dirname(sys.argv[0]) + "/.."
    sys.path.append(signs_dir)

    import tsp.rw.kgl as R
    main()
