#! /usr/bin/env python3
import sys
import os
import traceback
from sympy import sieve


def main():
    try:
        rdr = R.TspLib()
        data_in = rdr.read("docs/cities.tsp")
        tpl = separate_by_prime(data_in[1])

        hdr = data_in[0]
        # output prime file
        hdr_prime = R.TspLibHeader(
            hdr.name, hdr.comment, hdr.tsp_type,
            len(tpl[0]), hdr.edge_weight_type)
        rdr.write("docs/cities_prime.tsp", (hdr_prime, tpl[0]))

        # output non prime file
        hdr_not_prime = R.TspLibHeader(
            hdr.name, hdr.comment, hdr.tsp_type,
            len(tpl[1]), hdr.edge_weight_type)
        rdr.write("docs/cities_not_prime.tsp", (hdr_not_prime, tpl[1]))

        print("success")
    except Exception:
        traceback.print_exc()


def separate_by_prime(recs):
    sieve._reset()
    prime = []
    not_prime = []
    for rec in recs:
        if rec.node in sieve:
            prime.append(rec)
        else:
            not_prime.append(rec)

    assert len(prime) + len(not_prime) == len(recs), 'dropped records'
    return (prime, not_prime)


if __name__ == "__main__":
    signs_dir = os.path.dirname(sys.argv[0]) + "/.."
    sys.path.append(signs_dir)

    import tsp.rw.tsplib as R
    main()
