from typing import NamedTuple, List


class Point(NamedTuple):
    x: float
    y: float


def center_of_mass(ps: List[Point]) -> Point:
    w = 1 / len(ps)
    com = Point(x=0, y=0)
    for p in ps:
        com = Point(x=com.x + (w * p.x), y=com.y + (w * p.y))
    return com
