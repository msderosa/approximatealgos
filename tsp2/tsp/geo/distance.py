import math
import tsp.geo.point as T


def euclidean(a: T.Point, b: T.Point) -> float:
    return math.sqrt((a.x - b.x) ** 2 + (a.y - b.y) ** 2)
