from tsp.rw.rw_types import TspLibRec
from tsp.tactics.mod_tenblock_helper import ModTenBlockHelper
import tsp.geo.point as P
from typing import List, Optional, Tuple
from tsp.utils.loss import Loss
from enum import IntEnum
import random
import tsp.geo.distance as Dist


class Direction(IntEnum):
    Down = -1
    Up = 1


class ModTenBlock:
    _org_score: Optional[float]

    def __init__(self,
                 recs: List[TspLibRec],
                 prev_blk: Optional['ModTenBlock'],
                 loss: Loss) -> None:
        self._loss = loss
        self._acc = recs
        if prev_blk is not None:
            self._prev_blk_terminal = prev_blk.terminal()
        self._org_score = None

    def terminal(self)-> TspLibRec:
        return self._acc[-1]

    def initial(self) -> TspLibRec:
        temp = self._prev_blk_terminal
        assert temp is not None, "should be fully init before use"
        return temp

    def internal_rep(self) -> List[TspLibRec]:
        return self._acc.copy()

    def get_org_score(self) -> float:
        if self._org_score is None:
            self._org_score = self.score(self._acc)
        return self._org_score

    def set_prev_block(self, prev_blk: 'ModTenBlock') -> None:
        self._prev_blk_terminal = prev_blk.terminal()

    def set_prev_blk_terminal(self, rec: TspLibRec) -> None:
        self._prev_blk_terminal = rec

    def size(self) -> int:
        return len(self._acc)

    def optimize_lin_ker(self) -> Optional[List['ModTenBlock']]:
        rngs = self.logical_2l_ranges()
        optimizing_rngs = self.filter_logical_2l_ranges(rngs)
        best_acc = None
        best_score = 10.0 ** 6
        for rng in optimizing_rngs:
            acc = self.permute_by_logical_2l_range(rng)
            score = self.score(acc)
            if acc is None or score < best_score:
                best_acc = acc
                best_score = score

        blks = None
        if best_acc is not None and best_score < self.get_org_score():
            hd, rest = ModTenBlockHelper(self._loss).chunk_next(best_acc)
            prev_blk = ModTenBlock(hd, None, self._loss)
            prev_blk.set_prev_blk_terminal(self.initial())
            blks = [prev_blk]
            while len(rest) > 0:
                hd, rest = ModTenBlockHelper(self._loss).chunk_next(rest)
                blk = ModTenBlock(hd, prev_blk, self._loss)
                blks.append(blk)
                prev_blk = blk
        return blks

    def permute_by_logical_2l_range(
            self,
            rng: Tuple[int, int]) -> List[TspLibRec]:
        fst, snd = rng
        start = self._acc[:fst - 1]
        middle = self._acc[fst - 1:snd]
        middle.reverse()
        end = self._acc[snd:]
        temp = start + middle + end
        assert len(temp) == len(self._acc), \
            "partitioning leaves lengths unchanged"
        return temp

    def random_start_node(self) -> int:
        """ return a random logical start node for a range that will
        be reversed in the lk algo
        """
        min_rng_start = 1
        max_rng_start = len(self._acc) - 2
        rnd = random.randint(min_rng_start, max_rng_start)
        return rnd

    def get_by_logical_pos(self, n: int) -> TspLibRec:
        assert n >= 0, "underflow: {}".format(n)
        assert n <= len(self._acc), "overflow: {}".format(n)
        if n == 0:
            return self.initial()
        else:
            return self._acc[n - 1]

    def logical_2l_ranges(self) -> List[Tuple[int, int]]:
        e = len(self._acc)
        return [(a, b) for a in range(1, e) for b in range(1, e) if a < b]

    def filter_logical_2l_ranges(self, ns: List[Tuple[int, int]]):
        temp = []
        for start, end in ns:
            rec1 = self.get_by_logical_pos(start - 1)
            p1 = P.Point(x=rec1.x, y=rec1.y)
            rec2 = self.get_by_logical_pos(start)
            p2 = P.Point(x=rec2.x, y=rec2.y)
            rec3 = self.get_by_logical_pos(end)
            p3 = P.Point(x=rec3.x, y=rec3.y)
            rec4 = self.get_by_logical_pos(end + 1)
            p4 = P.Point(x=rec4.x, y=rec4.y)

            da = Dist.euclidean(p1, p2) + Dist.euclidean(p3, p4)
            db = Dist.euclidean(p1, p3) + Dist.euclidean(p2, p4)
            if (da > db):
                temp.append((start, end))
        return temp

    def score(self, blk: List[TspLibRec]) -> float:
        assert self.initial() is not None, "score state incomplete"
        assert self.initial() != blk[0], \
            "this convinience method works accumulated block parts only"
        ts = [self.initial()]
        ts.extend(blk)
        return self._loss.calculate_non_circuit(ts)

    def interval_scan_primes(self, start: int) -> List[int]:
        """ returns indices at which it finds primes
        """
        ps = []
        pos = start
        while pos < len(self._acc) - 1:
            if self._loss.is_prime_node(self._acc[pos]):
                ps.append(pos)
            pos = pos + 10
        return ps

    def swap_D(self, xs: List[TspLibRec], n: int, m: int) -> None:
        assert n < len(xs) and m < len(xs), \
            "overflow error, m: {}, n: {}, len: {}, last: {}".format(
                m, n, len(xs), xs[-1])
        temp = xs[n]
        xs[n] = xs[m]
        xs[m] = temp

    def split(self, recs: List[TspLibRec]) -> List['ModTenBlock']:
        """splits a recs, which has a prime node in a mod10 pos, into at
           most two blocks
        """
        assert len(recs) % 10 == 0, 'records not a multiple of 10'
        a_hd, a_rest = ModTenBlockHelper(self._loss).chunk_next(recs)
        a_blk = ModTenBlock(a_hd, None, self._loss)
        a_blk.set_prev_blk_terminal(self.initial())
        if len(a_rest) > 0:
            b_hd, b_rest = ModTenBlockHelper(self._loss).chunk_next(a_rest)
            b_blk = ModTenBlock(b_hd, a_blk, self._loss)
            assert b_rest == [], 'a split uses all blocks'
            assert self.size() == a_blk.size() + b_blk.size(), "split does not account for all records"
            assert a_blk.terminal() == b_blk.initial(), "unconnected split"
            return [a_blk, b_blk]
        else:
            assert self.size() == a_blk.size(), "split does not account for all records"
            return [a_blk]

    def optimize_swap(
            self,
            direction: Direction) -> Optional[List['ModTenBlock']]:
        """move prime from 9mod (up) or 1mod (down) pos to 10mod pos
        """
        if len(self._acc) == 10:
            return None

        replacement = None
        ps = self.interval_scan_primes(9 - int(direction))
        for pos in ps:
            copy = self._acc.copy()
            self.swap_D(copy, pos, pos + int(direction))
            new_score = self.score(copy)
            if new_score <= self.get_org_score():
                replacement = self.split(copy)
                break
        return replacement
