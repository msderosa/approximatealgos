from tsp.rw.rw_types import TspLibRec
from tsp.tactics.tenblock import TenBlock
from typing import List
from tsp.utils.loss import Loss


class TenBlockList:

    def __init__(self, loss: Loss, tour: List[TspLibRec]) -> None:
        self._org_tour = tour
        self._loss = loss

    def block(self) -> List[TenBlock]:
        bs: List[TenBlock] = []
        idx = 10
        while idx < len(self._org_tour):
            curr = self._org_tour[idx:idx + 11]
            prev = self._org_tour[idx - 10:idx + 1]
            blk = TenBlock(self._loss, curr, prev)
            bs.append(blk)
            idx = idx + 10
        return bs

    def unblock(self, bs: List[TenBlock]) -> List[TspLibRec]:
        recs = self._org_tour[0:11]
        for blk in bs:
            (c, p) = blk.get_optimized_values()
            if c is None:
                (org_c, org_p) = blk.get_unoptimized_values()
                recs = recs + org_c[1:]
            else:  # backtrack a bit, both curr and prev have been changed
                assert c is not None and p is not None, 'not none always holds'
                recs = recs[:-11] + p + c[1:]

        assert len(self._org_tour) == len(recs), 'tour length mismatch'
        return recs

    def optimize(self) -> None:
        blks = self.block()
        rolling_update = None
        for blk in blks:
            if rolling_update is not None:
                blk.update_prev(rolling_update)
            blk.optimize()
            (c, p) = blk.get_optimized_values()
            rolling_update = c

        self._opt_tour = self.unblock(blks)
