from typing import NamedTuple, Tuple
from tsp.rw.rw_types import TspLibRec


class MinInsertCost(NamedTuple):
    low_node: TspLibRec
    low_cost: float
    high_node: TspLibRec
    high_cost: float


InsertablePrime = Tuple[TspLibRec, MinInsertCost]
