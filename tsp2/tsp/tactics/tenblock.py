from tsp.rw.rw_types import TspLibRec
from typing import List, Optional, Tuple
from tsp.utils.loss import Loss


class TenBlock:

    def __init__(self, loss: Loss, curr: List[TspLibRec],
                 prev: List[TspLibRec]) -> None:
        assert len(curr) <= 11, 'previous block invalid length'
        assert len(prev) == 11, 'previous block invalid length'
        assert prev[10].node == curr[0].node, \
            "blocks not adjacent {} <> {}".format(prev[10].node, curr[0].node)
        self._loss = loss

        self._curr = curr
        self._prev = prev
        self._org_score = self.score(self._curr) + self.score(self._prev)

        self._opt_curr: Optional[List[TspLibRec]] = None
        self._opt_prev: Optional[List[TspLibRec]] = None
        self._opt_score: Optional[float] = None

    def update_prev(self, p):
        assert len(p) == 11, 'incorrect length of prev'
        assert (p[0].node != self._prev[0].node), \
            'there should be change for prev to be updated'
        assert self._loss.is_prime_node(p[0]), \
            'prev node updated only if it is prime'
        assert p[10].node == self._curr[0].node, \
            "blocks not adjacent {} <> {}".format(p[10].node, self._curr[0].node)

        self._prev = p

    def score(self, blk: List[TspLibRec]) -> float:
        return self._loss.calculate_non_circuit(blk)

    def swap_right(self, pos: int) -> Tuple[List[TspLibRec], List[TspLibRec]]:
        assert pos >= 0, 'unexpected right position'
        curr = self._curr.copy()
        prev = self._prev.copy()

        curr[0] = self._curr[pos]
        curr[pos] = self._curr[0]
        prev[-1] = curr[0]
        assert prev[10].node == curr[0].node, 'blocks are not adjacent'
        return (curr, prev)

    def swap_left(self, pos: int) -> Tuple[List[TspLibRec], List[TspLibRec]]:
        assert pos >= 0, 'unexpected left position'
        curr = self._curr.copy()
        prev = self._prev.copy()

        prev[-1] = self._prev[pos]
        prev[pos] = self._prev[-1]
        curr[0] = prev[-1]
        assert prev[10].node == curr[0].node, 'blocks are not adjacent'
        return (curr, prev)

    def index_first_prime_right(self) -> Optional[int]:
        pos = None
        for idx in range(0, len(self._curr) - 1):
            rec = self._curr[idx]
            if self._loss.is_prime_node(rec):
                pos = idx
                break
        return pos

    def index_first_prime_left(self) -> Optional[int]:
        pos = None
        for idx in range(0, 10):
            rec = self._prev[9 - idx]
            if self._loss.is_prime_node(rec):
                pos = 9 - idx
                break
        return pos

    def is_low_score(self, score: float) -> bool:
        return (score < self._org_score and self._opt_score is None) \
          or (score < self._org_score and self._opt_score is not None and score < self._opt_score)

    def set_optimized_values(self, score: float,
                             curr: List[TspLibRec],
                             prev: List[TspLibRec]) -> None:
        assert self._opt_score is None or score < self._opt_score, \
          'score values are increase only variants'
        self._opt_curr = curr
        self._opt_prev = prev
        self._opt_score = score

    def get_optimized_values(self) -> Tuple[
            Optional[List[TspLibRec]], Optional[List[TspLibRec]]]:
        return (self._opt_curr, self._opt_prev)

    def get_unoptimized_values(self) -> Tuple[
            List[TspLibRec], List[TspLibRec]]:
        return (self._curr, self._prev)

    def optimize(self) -> None:
        if self._loss.is_prime_node(self._curr[0]):
            return

        idx_r = self.index_first_prime_right()
        if idx_r is not None:
            (c_r, p_r) = self.swap_right(idx_r)
            score = self.score(c_r) + self.score(p_r)
            if self.is_low_score(score):
                self.set_optimized_values(score, c_r, p_r)

        idx_l = self.index_first_prime_left()
        if idx_l is not None:
            (c_l, p_l) = self.swap_left(idx_l)
            score = self.score(c_l) + self.score(p_l)
            if self.is_low_score(score):
                self.set_optimized_values(score, c_l, p_l)
