from typing import List, Dict
from tsp.rw.rw_types import TspLibRec
from tsp.tactics.tactic_types import MinInsertCost, InsertablePrime


class PrimeBacklog:

    def __init__(self) -> None:
        self._insert_pos_to_prime: Dict[int, List[InsertablePrime]] = {}
        self._index_prime_to_pos: Dict[int, int] = {}

    def size(self):
        return len(self._insert_pos_to_prime)

    def add(self, rec: TspLibRec, cost: MinInsertCost) -> None:
        insert_pos: int = cost.high_node.node
        if insert_pos in self._insert_pos_to_prime:
            tpls = self._insert_pos_to_prime[insert_pos]
            if self._contains(tpls, rec) is False:
                tpls.append((rec, cost))
                self._index_prime_to_pos[rec.node] = insert_pos
        else:
            self._insert_pos_to_prime[insert_pos] = [(rec, cost)]
            self._index_prime_to_pos[rec.node] = insert_pos

    def exists_at(self, rec: TspLibRec, insert_pos: int) -> bool:
        exists = False
        if insert_pos in self._insert_pos_to_prime:
            tpls = self._insert_pos_to_prime[insert_pos]
            exists = self._contains(tpls, rec)
        return exists

    def _contains(self,
                  tpls: List[InsertablePrime], rec: TspLibRec) -> bool:
        tf = False
        for tpl in tpls:
            if (tpl[0].node == rec.node):
                tf = True
                break
        return tf

    def delete(self, rec: TspLibRec) -> None:
        if rec.node not in self._index_prime_to_pos:
            return

        cnt1 = len(self._index_prime_to_pos)
        del_tpl = None
        insert_pos = self._index_prime_to_pos[rec.node]
        tpls = self._insert_pos_to_prime[insert_pos]
        for tpl in tpls:
            if tpl[0].node == rec.node:
                del_tpl = tpl
                break
        assert del_tpl is not None, "did not find expected delete items"
        tpls.remove(del_tpl)
        if len(tpls) == 0:
            del self._insert_pos_to_prime[insert_pos]
        del self._index_prime_to_pos[rec.node]
        cnt2 = len(self._index_prime_to_pos)
        assert cnt1 > cnt2, "delete did not remove and elem from backlog"

    def get_waiting_inserts(
            self, chunk: List[TspLibRec]) -> List[InsertablePrime]:
        assert len(chunk) <= 10, "chunk has a max of 9 elements"

        ws = []
        for rec in chunk:
            if rec.node in self._insert_pos_to_prime:
                ws.extend(self._insert_pos_to_prime[rec.node])
        return ws
