from typing import List, Dict, Tuple, Union, Iterable, Set
from tsp.rw.rw_types import TspLibRec
import scipy.spatial as Sp
import tsp.utils.relations as R
import tsp.geo.point as P
import numpy as np


class IndexedNodeSet:

    def __init__(self, recs: List[TspLibRec]) -> None:
        self._do_init(recs)

    def _do_init(self, recs: List[TspLibRec]) -> None:
        self._recs = recs
        self._kdtree = Sp.KDTree([[p.x, p.y] for p in recs])
        self._rec_to_index = self.mk_node_to_index(recs)
        self._used: Set[TspLibRec] = set()

    def size(self) -> int:
        return len(self._recs)

    def mk_node_to_index(self, recs: List[TspLibRec]) -> Dict[int, int]:
        index = {}
        for idx in range(0, len(recs)):
            rec = recs[idx]
            index[rec.node] = idx
        return index

    def neighbor_low(self, rec: TspLibRec) -> TspLibRec:
        idx = self._rec_to_index[rec.node]
        return self._recs[idx - 1]

    def neighbor_high(self, rec: TspLibRec) -> TspLibRec:
        idx = self._rec_to_index[rec.node]
        idx_r = idx + 1
        if idx_r >= len(self._recs):
            idx_r = 0
        return self._recs[idx_r]

    def mark_as_used(self, rec: TspLibRec) -> None:
        if rec.node in self._rec_to_index:
            r, d = self.nearest(rec)
            assert r.node == rec.node, "the given node is not in the tree"
            sz = len(self._used)
            self._used.add(rec)
            assert sz + 1 == len(self._used), \
                "added element twice? {}".format(r)

    def nearest_unused(self, rec: TspLibRec) -> Tuple[TspLibRec, float]:
        answer = None
        for n in range(1, 10):
            ps = self.n_nearest(rec, n)
            filtered = [e for e in ps if e[0] not in self._used]
            if len(filtered) > 1:
                assert False, "filtered should only be 0 or 1 in length"
            elif len(filtered) == 1:
                answer = filtered[0]
                break
        assert answer is not None, "underflow nearest not found"

        if n >= 5:
            self._rebuild()
        return answer

    def _rebuild(self) -> None:
        ls = [e for e in self._recs if e not in self._used]
        self._do_init(ls)

    def nearest(self, rec: TspLibRec) -> Tuple[TspLibRec, float]:
        pnt = [rec.x, rec.y]
        dist, idx = self._kdtree.query(pnt)
        return (self._recs[idx], dist)

    def second_nearest(self, rec: TspLibRec) -> Tuple[TspLibRec, float]:
        pnt = [rec.x, rec.y]
        ds, idxs = self._kdtree.query(pnt, k=2)
        if ds[0] <= ds[1]:
            return (self._recs[idxs[1]], ds[1])
        else:
            return (self._recs[idxs[0]], ds[0])

    def n_nearest(self, rec: Union[TspLibRec, P.Point], n: int) -> List[
            Tuple[TspLibRec, float]]:
        assert n > 0, "can have n = 0"
        pnt = [rec.x, rec.y]
        ds, idxs = self._kdtree.query(pnt, k=n)
        if n == 1:
            return [(self._recs[idxs], ds)]
        else:
            return [(self._recs[tpl[0]], tpl[1]) for tpl in zip(idxs, ds)]

    def nearest_to_both_and_unused(
            self, pnt9: TspLibRec,
            pnt10: TspLibRec) -> Tuple[TspLibRec, float]:
        search = True
        k = 1
        while search:
            cs = self.n_nearest_to_both(pnt9, pnt10, k)
            cs2 = [(rec, d) for (rec, d) in cs
                   if rec not in self._used]
            if len(cs2) == 0:
                k = k + 1
            else:
                search = False
        return self.min_distance(cs2)

    # returns the prime node and a float, that is the added distance
    # introduced by inserting the prime
    def nearest_to_both(self, pnt9: TspLibRec,
                        pnt10: TspLibRec) -> Tuple[TspLibRec, float]:
        search = True
        k = 1
        while search:
            cs = self.n_nearest_to_both(pnt9, pnt10, k)
            if len(cs) == 0:
                k = k + 1
            else:
                search = False
        return self.min_distance(cs)

    def n_nearest_to_both(self, pnt9: TspLibRec,
                          pnt10: TspLibRec,
                          n: int) -> List[Tuple[TspLibRec, float]]:
        ds9, ns9 = self._kdtree.query([pnt9.x, pnt9.y], k=n)
        ds10, ns10 = self._kdtree.query([pnt10.x, pnt10.y], k=n)
        cs = self.join(ns10, ds10, ns9, ds9)
        ds = [(self._recs[nd], dl + dr) for (nd, dl, dr) in cs]
        assert len(ds) <= n, "limited by n but may be less"
        return ds

    def join(
            self,
            ns10: Union[Iterable[int], int],
            ds10: Union[Iterable[float], float],
            ns9: Union[Iterable[int], int],
            ds9: Union[Iterable[float], float]) -> List[Tuple[int, float, float]]:
        t = type(ns9)
        if t == np.ndarray:
            return R.join(zip(ns10, ds10), zip(ns9, ds9))  # type: ignore
        else:
            return R.join(zip([ns10], [ds10]), zip([ns9], [ds9]))  # type: ignore

    # returns the node number of the nearest prime and the left and right
    # insert distance
    def min_distance(
            self,
            cs: List[Tuple[TspLibRec, float]]) -> Tuple[TspLibRec, float]:
        assert len(cs) > 0, 'can not select a min from an empty list'
        curr_min = None
        for c in cs:
            if curr_min is None:
                curr_min = c
            elif curr_min[1] > c[1]:
                curr_min = c
        assert curr_min is not None, 'must have a value if list is not empty'
        return curr_min
