from tsp.rw.rw_types import TspLibRec
from typing import List, Tuple
from tsp.utils.loss import Loss


class ModTenBlockHelper:

    def __init__(self, loss: Loss) -> None:
        self._loss = loss
        self._acc: List[TspLibRec] = []

    def chunk_next(self, recs: List[TspLibRec]
                   ) -> Tuple[List[TspLibRec], List[TspLibRec]]:

        tail = self._inner_chunk_next(recs)

        assert len(self._acc) + len(tail) == len(recs), "len of two sublists equals len of parent"
        if self._acc[-1].node != 0:
            assert len(self._acc) % 10 == 0, "blocks sizes are moulo 10 except at the end of a circuit: len = {}".format(len(self._acc))
        if len(tail) > 0:
            assert self._acc[-1] != tail[0], "sub blocks of a parent are disjoint"
        return (self._acc, tail)

    def _inner_chunk_next(self, recs: List[TspLibRec]) -> List[TspLibRec]:
        head = recs[0:10]
        tail = recs[10:]
        self._acc.extend(head)
        if len(head) < 10:
            return tail
        elif self._loss.is_prime_node(head[-1]):
            return tail
        else:
            return self._inner_chunk_next(tail)
