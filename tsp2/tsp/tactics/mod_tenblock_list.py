from tsp.rw.rw_types import TspLibRec
from tsp.tactics.mod_tenblock import ModTenBlock, Direction
from tsp.tactics.mod_tenblock_helper import ModTenBlockHelper
from typing import List
from tsp.utils.loss import Loss


class ModTenBlockList:

    def __init__(self, tour: List[TspLibRec], loss: Loss) -> None:
        assert tour[0].node == 0, \
            "expecting all tours to start with the 0 node: {}".format(
                tour[0].node)
        self._org_tour = tour
        self._loss = loss
        self._blks = self.block()

    def size(self) -> int:
        return len(self._blks)

    def at(self, n: int) -> ModTenBlock:
        assert n < self.size(), "out of bounds, overflow"
        assert n >= 0, "out of bounds, underflow"
        return self._blks[n]

    def block(self) -> List[ModTenBlock]:
        blks: List[ModTenBlock] = []
        tail = self._org_tour[1:]
        tail.append(self._org_tour[0])

        prev_blk = None
        while len(tail) > 0:
            h = ModTenBlockHelper(self._loss)
            hd, tail = h.chunk_next(tail)
            blk = ModTenBlock(hd, prev_blk, self._loss)
            blks.append(blk)
            prev_blk = blk
        blks[0].set_prev_block(blk)
        return blks

    def unblock(self) -> List[TspLibRec]:
        fst = self._blks[0].initial()
        blks = [fst]
        for blk in self._blks:
            blks.extend(blk.internal_rep())

        assert len(blks) == len(self._org_tour) + 1, \
            "tour length can not change: is {} was {}".format(
                len(blks), len(self._org_tour))
        assert blks[0] == self._org_tour[0], \
            "tour origin can not change"
        return blks[:-1]

    def optimize(self) -> None:
        self.optimize_swap(Direction.Up)
        print("swap up: ", self.size())
        self.optimize_swap(Direction.Down)
        print("swap down: ", self.size())
        self.optimize_lin_ker()
        print("lin ker 1: ", self.size())

    def optimize_swap(self, drctn: Direction) -> None:
        bs = []
        for blk in self._blks:
            pr = blk.optimize_swap(drctn)
            if pr is None:
                bs.append(blk)
            else:
                bs.extend(pr)
        self._blks = bs

    def optimize_lin_ker(self) -> None:
        bs = []
        for blk in self._blks:
            opt_blks = blk.optimize_lin_ker()
            if opt_blks is None:
                bs.append(blk)
            else:
                opt_opt_blks = self.rec_optimize_lin_ker(opt_blks)
                bs.extend(opt_opt_blks)
        self._blks = bs

    def rec_optimize_lin_ker(self, blks:
                             List[ModTenBlock]) -> List[ModTenBlock]:
        if len(blks) == 0:
            return []

        hd_blk = blks[0]
        tl_blks = blks[1:]
        opt_blks = hd_blk.optimize_lin_ker()
        if opt_blks is None:
            temp = self.rec_optimize_lin_ker(tl_blks)
            temp.insert(0, hd_blk)
            return temp
        else:
            temp = opt_blks + tl_blks
            return self.rec_optimize_lin_ker(temp)
