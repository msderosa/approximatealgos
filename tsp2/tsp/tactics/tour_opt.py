from tsp.rw.rw_types import TspLibRec
from tsp.tactics.mod_tenblock_list import ModTenBlockList
from tsp.utils.loss import Loss
from typing import List, Set


# In [2]: cnt_nprimes = 179967
# In [3]: cnt_primes = 17802
# ----------- edge length stats -------
# avg:  7.746003630348002 (and so avg penalty of a non placement)
# max:  180.04783366562322
# min:  1.3738105710721984
# median:  6.706823267729655
# ----------- log edge length stats -------
# avg:  0.6534711828870008
# max:  1.6473544212851954
# min:  -1.1469993338625735
# median:  0.6434974854332982
# 1.1 times more than twice the average internode distance.
InsertLimit = 2.75 * 7.746
LogInsertLimit = 2 * 0.6534711828870008


class InsertPrime:

    def __init__(self, loss: Loss, tour: List[TspLibRec],
                 primes: Set[TspLibRec]) -> None:
        self._loss = loss
        self._blk_list = ModTenBlockList(tour, loss)
