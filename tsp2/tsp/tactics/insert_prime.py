from tsp.rw.rw_types import TspLibRec
from tsp.tactics.indexed_node_set import IndexedNodeSet
from tsp.tactics.prime_backlog import PrimeBacklog
from tsp.tactics.tactic_types import MinInsertCost, InsertablePrime
from tsp.utils.loss import Loss
from typing import List, Tuple, Set
import math
import tsp.geo.distance as Dist
import tsp.geo.point as P
import tsp.stats.circuit_stats as Cs
import tsp.stats.summary_stats as Ss

# ----------- edge length stats -------
# avg:  7.746003630348002 (and so avg penalty of a non placement)
# max:  180.04783366562322
# min:  1.3738105710721984
# median:  6.706823267729655
# ----------- log edge length stats -------
# avg:  0.6534711828870008
# max:  1.6473544212851954
# min:  -1.1469993338625735
# median:  0.6434974854332982
# 1.1 times more than twice the average internode distance.
InsertLimit = 2.75 * 7.746
LogInsertLimit = 2 * 0.6534711828870008


class InsertPrime:

    def __init__(self, loss: Loss, tour: List[TspLibRec],
                 primes: Set[TspLibRec]) -> None:
        self._loss = loss

        self._prime_node_set = IndexedNodeSet(list(primes))
        self._tour_node_set = IndexedNodeSet(tour)
        self._mod_tour = tour.copy()
        self.bklog = PrimeBacklog()

    def prime_min_insert_cost(self, rec: TspLibRec) -> MinInsertCost:
        low = self.prime_insert_cost_low_side(rec)
        high = self.prime_insert_cost_high_side(rec)
        if (low.low_cost + low.high_cost) < (high.low_cost + high.high_cost):
            return low
        else:
            return high

    def prime_insert_cost_high_side(self, rec: TspLibRec) -> MinInsertCost:
        n, dist = self._tour_node_set.nearest(rec)
        n_high = self._tour_node_set.neighbor_high(n)
        return MinInsertCost(
            low_node=n,
            low_cost=Dist.euclidean(P.Point(x=rec.x, y=rec.y),
                                    P.Point(x=n.x, y=n.y)),
            high_node=n_high,
            high_cost=Dist.euclidean(P.Point(x=rec.x, y=rec.y),
                                     P.Point(x=n_high.x, y=n_high.y))
            )

    def prime_insert_cost_low_side(self, rec: TspLibRec) -> MinInsertCost:
        n, dist = self._tour_node_set.nearest(rec)
        n_low = self._tour_node_set.neighbor_low(n)
        return MinInsertCost(
            low_node=n_low,
            low_cost=Dist.euclidean(P.Point(x=rec.x, y=rec.y),
                                    P.Point(x=n_low.x, y=n_low.y)),
            high_node=n,
            high_cost=Dist.euclidean(P.Point(x=rec.x, y=rec.y),
                                     P.Point(x=n.x, y=n.y))
            )

    def avg_log_distance_to_base_tour(self, primes: Set[TspLibRec]) -> float:
        ds = []
        for p in primes:
            nd, d = self._tour_node_set.nearest(p)
            ds.append(math.log(d))
        return Ss.avg(ds)

    def split_by_avg_distance_to_base_circuit(
            self, primes: Set[TspLibRec]) -> Tuple[
                Set[TspLibRec], Set[TspLibRec]]:
        d_avg_log = self.avg_log_distance_to_base_tour(primes)
        below_avg = set()
        above_avg = set()
        for p in primes:
            assert self._loss.is_prime_node(p), \
                "non prime node in split by avg diff: {}".format(p)
            nd, d = self._tour_node_set.nearest(p)
            if math.log(d) < d_avg_log:
                below_avg.add(p)
            else:
                above_avg.add(p)
        assert len(primes) == len(below_avg) + len(above_avg), "len mismatch"
        return (below_avg, above_avg)

    def graph(self) -> None:
        ls = Cs.internode_distances(self._mod_tour)
        Cs.print_stats(ls)

        ls_log = [math.log(e) for e in ls]
        Cs.print_stats(ls_log)

        Cs.histogram(ls_log)

    def prime_backlog(self, primes: Set[TspLibRec]) -> None:
        for p in primes:
            assert self._loss.is_prime_node(p), \
                "non prime node added to backlog: {}".format(p)
            mic = self.prime_min_insert_cost(p)
            self.bklog.add(p, mic)

    def optimize(self) -> List[TspLibRec]:
        print('optimizing...')
        acc = [self._mod_tour[0]]
        tail = self._mod_tour[1:]

        while len(tail) > 0:
            chunk = tail[0: 10]
            tail = tail[10:]
            residual: List[TspLibRec] = []
            assert len(chunk) <= 10, 'chunks are 10 items, except last chunk'

            chunk = self.merge_backlogs_to_chunk(self.bklog, chunk)
            chunk, residual = self.partition_chunk_and_residual(
                chunk, residual)
            if len(chunk) < 10 or self._loss.is_prime_node(chunk[9]):
                acc.extend(chunk)
            else:
                rec, dist = self._prime_node_set.nearest_to_both_and_unused(
                    chunk[8], chunk[9])
                if dist < InsertLimit:
                    chunk.insert(9, rec)
                    self._prime_node_set.mark_as_used(rec)
                    self.bklog.delete(rec)
                    chunk, residual = self.partition_chunk_and_residual(
                        chunk, residual)
                else:
                    mic = self.prime_min_insert_cost(rec)
                    self.bklog.add(rec, mic)
                acc.extend(chunk)
            # self.debug_no_duplicates(acc)
            tail = residual + tail

        return acc

    def partition_chunk_and_residual(
            self,
            recs: List[TspLibRec],
            resdl: List[TspLibRec]) -> Tuple[List[TspLibRec], List[TspLibRec]]:
        if len(recs) == 10:
            return (recs, resdl)
        else:
            return (recs[0:10], resdl + recs[10:])

    def merge_backlogs_to_chunk(self, backlog: PrimeBacklog,
                                chunk: List[TspLibRec]) -> List[TspLibRec]:
        primes = backlog.get_waiting_inserts(chunk)

        chunk_latest = chunk

        for prime_data in primes:
            chunk_latest = self.merge_backlog_to_chunk(
                chunk_latest,
                prime_data)
            self._prime_node_set.mark_as_used(prime_data[0])
            backlog.delete(prime_data[0])
        return chunk_latest

    def merge_backlog_to_chunk(
            self, chunk: List[TspLibRec],
            prime_data: InsertablePrime) -> List[TspLibRec]:
        new_chunk = []
        prime, data = prime_data
        for blk in chunk:
            if data.high_node.node == blk.node:
                new_chunk.append(prime)
            new_chunk.append(blk)

        assert len(new_chunk) > len(chunk), "expected to add something"
        return new_chunk

    def investigate1(self):
        # what number of nodes are beyond the average regardess of insert pnt
        xs = []
        for rec in self._prime_node_set._recs:
            rec_dist1 = self._tour_node_set.nearest(rec)
            rec_dist2 = self._prime_node_set.second_nearest(rec)
            if (min(rec_dist1[1], rec_dist2[1]) > InsertLimit / 2):
                xs.append(rec)
        print("total primes: ", len(self._prime_node_set._recs))
        print("primes beyond limit: ", len(xs))

# In [2]: cnt_nprimes = 179967
# In [3]: cnt_primes = 17802
