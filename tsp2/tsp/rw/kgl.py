import csv
from typing import List, Tuple
from datetime import datetime


class Kgl:

    def read(self) -> List[Tuple[int, float, float]]:
        flds: List[Tuple[int, float, float]] = []
        with open('docs/cities.csv', newline='') as fd:
            flds = self.extract_fields(fd)
        return flds

    def extract_fields(self, fd) -> List[Tuple[int, float, float]]:
        flds = []
        rdr = csv.reader(fd)
        rdr.__next__()
        for rec in rdr:
            flds.append(self.extract_field(rec))
        return flds

    def extract_field(self, rec: List[str]) -> Tuple[int, float, float]:
        number = int(rec[0])
        x = float(rec[1])
        y = float(rec[2])
        return (number, x, y)

    def write(self, flds: List[Tuple[int, float, float]]) -> None:
        dt = datetime.now().isoformat(timespec='minutes')
        with open("target/path_{}.csv".format(dt), 'w') as fd:
            fd.write("Path\n")
            for fld in flds:
                fd.write("{}\n".format(fld[0]))
