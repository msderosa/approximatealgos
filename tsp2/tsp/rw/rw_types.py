from typing import NamedTuple


class TspLibHeader(NamedTuple):
    name: str
    comment: str
    tsp_type: str
    dimension: int
    edge_weight_type: str


class TspLibRec(NamedTuple):
    node: int
    x: float
    y: float
