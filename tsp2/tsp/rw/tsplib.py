from typing import List, Tuple, IO
from tsp.rw.rw_types import TspLibHeader, TspLibRec


class TspLib:

    def read_tour(self, file_name: str) -> List[int]:
        xs = []
        with open(file_name, 'r') as fd:
            in_tour_section = False
            for rec in fd:
                if in_tour_section:
                    n = int(rec.strip())
                    if n < 0:
                        in_tour_section = False
                    else:
                        xs.append(n)
                elif rec.startswith("TOUR_SECTION"):
                    in_tour_section = True
        return xs

    def write(self, file_name: str,
              data: Tuple[TspLibHeader, List[TspLibRec]]) -> None:
        head = data[0]
        content = data[1]
        assert head.dimension == len(content), \
            "mismatch in dimensions during tsplib write"
        with open(file_name, 'w') as fd:
            self.write_header(fd, head)
            self.write_node_coord_section(fd, content)

        with open(self.normed_name(file_name), 'w') as fd:
            self.write_header(fd, head)
            self.write_node_coord_section_normed(fd, content)

    def normed_name(self, file_name: str) -> str:
        parts = file_name.split('.')
        return parts[0] + '_normed.' + parts[1]

    def write_node_coord_section(self, hdl: IO[str],
                                 recs: List[TspLibRec]) -> None:
        hdl.write("NODE_COORD_SECTION\n")
        for rec in recs:
            hdl.write("{} {} {}\n".format(rec.node, rec.x, rec.y))
        hdl.write("EOF\n")

    def write_node_coord_section_normed(self, hdl: IO[str],
                                        recs: List[TspLibRec]) -> None:
        hdl.write("NODE_COORD_SECTION\n")
        index = 0
        for rec in recs:
            index = index + 1
            hdl.write("{} {} {}\n".format(index, rec.x, rec.y))
        hdl.write("EOF\n")

    def write_header(self, hdl: IO[str], header: TspLibHeader) -> None:
        hdl.write("NAME : {}\n".format(header.name))
        hdl.write("COMMENT : {}\n".format(header.comment))
        hdl.write("TYPE : {}\n".format(header.tsp_type))
        hdl.write("DIMENSION : {}\n".format(header.dimension))
        hdl.write("EDGE_WEIGHT_TYPE : {}\n".format(header.edge_weight_type))

    def read(self, file_name: str) -> Tuple[TspLibHeader, List[TspLibRec]]:
        with open(file_name, 'r') as fd:
            header = self.read_header(fd)
            recs = self.read_node_coord_section(fd)
            assert header.dimension == len(recs), \
                "mismatch between header dimension and contents"
        return (header, recs)

    def read_node_coord_section(self, hdl: IO[str]) -> List[TspLibRec]:
        lns: List[TspLibRec] = []
        for ln in hdl:
            if ln == '' or ln.startswith('EOF') or ln == '\n' or ln.startswith('-1'):
                break

            lns.append(self.parse_node_coord(ln))
        return lns

    def parse_node_coord(self, ln) -> TspLibRec:
        parts = ln.split(' ')
        assert len(parts) == 3, \
            "incorrect element count in node coord section: {}".format(ln)
        assert parts[0].isdigit(), "incorrect node number: {}".format(parts[0])
        return TspLibRec(
            node=int(parts[0]),
            x=float(parts[1]),
            y=float(parts[2]))

    def read_header(self, hdl: IO[str]) -> TspLibHeader:
        name = ''
        comment = ''
        tsp_type = ''
        dimension = 0
        edge_weight_type = ''
        for ln in hdl:
            assert ln != '', 'hit EOF when trying to get header'
            if ln.startswith('NODE_COORD_SECTION'):
                break

            hdr_parts = self.parse_header_line(ln)
            key = hdr_parts[0]
            if key == 'NAME':
                name = hdr_parts[1]
            elif key == 'COMMENT':
                comment = hdr_parts[1]
            elif key == 'TYPE':
                tsp_type = hdr_parts[1]
            elif key == 'DIMENSION':
                assert hdr_parts[1], 'non digit dimension in header'
                dimension = int(hdr_parts[1])
            elif key == 'EDGE_WEIGHT_TYPE':
                edge_weight_type = hdr_parts[1]
            else:
                raise Exception("unknown header key: {}".format(key))

        return TspLibHeader(name, comment, tsp_type, dimension,
                            edge_weight_type)

    def parse_header_line(self, item: str) -> List[str]:
        ps1 = item.split(':')
        ps2 = [el.strip() for el in ps1]
        assert len(ps2) == 2, \
            "unexpected length of header parse items: {}".format(item)
        return ps2
