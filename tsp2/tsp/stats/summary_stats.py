from typing import List
import math


def avg(ns: List[float]) -> float:
    return sum(ns)/len(ns)


def median(ns: List[float]) -> float:
    ms = ns.copy()
    ms.sort()
    mid_point = math.floor(len(ms) / 2)
    return ms[mid_point]
