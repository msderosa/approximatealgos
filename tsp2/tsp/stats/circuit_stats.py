from tsp.rw.rw_types import TspLibRec
import tsp.geo.distance as Dist
from tsp.geo.point import Point
from typing import List
import matplotlib.pyplot as plt
import tsp.stats.summary_stats as Stats


def internode_distances(recs: List[TspLibRec]) -> List[float]:
    temp = []
    for idx in range(1, len(recs)):
        rec1 = recs[idx - 1]
        pnt1 = Point(x=rec1.x, y=rec1.y)
        rec2 = recs[idx]
        pnt2 = Point(x=rec2.x, y=rec2.y)
        d = Dist.euclidean(pnt1, pnt2)
        temp.append(d)
    return temp


def print_stats(temp: List[float]) -> None:
    print("avg: ", sum(temp)/len(temp))
    print("max: ", max(temp))
    print("min: ", min(temp))
    print("median: ", Stats.median(temp))


def histogram(data: List[float]) -> None:
    fig, ax = plt.subplots()
    ax.hist(data, bins=1000, normed=True)
    plt.show()
