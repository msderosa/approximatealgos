from tsp.rw.rw_types import TspLibRec
from tsp.tactics.mod_tenblock import ModTenBlock
from typing import List, Tuple, Set
import math
import matplotlib.pyplot as plt
import statistics


def run_stats(blks: List[ModTenBlock], primes: Set[TspLibRec]) -> None:
    ns = [blk.size() for blk in blks]
    print_stats(ns)
    histogram(ns)

    ms = [math.log(blk.get_org_score() / blk.size()) for blk in blks]
    ms_sts = print_stats(ms)
    histogram(ms)
    plot_selected(blks, ms_sts, primes)


Stats = Tuple[float, float, float, float, float]


def print_stats(temp: List[float]) -> Stats:
    _avg = sum(temp)/len(temp)
    _max = max(temp)
    _min = min(temp)
    _median = statistics.median(temp)
    _stdev = statistics.stdev(temp)

    print("avg: ", _avg)
    print("max: ", _max)
    print("min: ", _min)
    print("median: ", _median)
    print("std dev: ", _stdev)
    return (_avg, _max, _min, _median, _stdev)


def histogram(data: List[float]) -> None:
    fig, ax = plt.subplots()
    ax.hist(data, bins=1000, normed=True)
    plt.show()


def plot_selected(blks: List[ModTenBlock], sts: Stats,
                  primes: Set[TspLibRec]) -> None:
    high_blks = [blk for blk in blks
                 if math.log(blk.get_org_score() / blk.size()) > sts[0] + 3 * sts[4]]

    n = min(20, len(high_blks))
    while n > 0:
        stour1 = high_blks[-1 * n].internal_rep()
        plt.plot([n.x for n in stour1], [n.y for n in stour1])
        n = n - 1
    plt.scatter([n.x for n in primes], [n.y for n in primes])
    plt.show()
