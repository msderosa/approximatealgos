from sympy import sieve
from tsp.rw.rw_types import TspLibRec
from typing import List, Tuple
import tsp.geo.distance as Dist
import tsp.geo.point as T


class Loss:

    def __init__(self):
        sieve._reset()

    def is_prime_node(self, n: TspLibRec) -> bool:
        return n.node == 0 or n.node in sieve

    def calc_penalty(self, rec: TspLibRec) -> float:
        if self.is_prime_node(rec):
            return 1.0
        else:
            return 1.1

    def calculate_non_circuit(self, recs: List[TspLibRec]) -> float:
        penalty = self.calc_penalty(recs[0])
        edges = zip(recs[0:-1], recs[1:])
        loss = 0.0
        iteration = 1
        for edge in edges:
            d = self.edge_distance(edge)
            loss = loss + (penalty * d)

            if iteration % 10 == 0:
                penalty = self.calc_penalty(edge[1])
            iteration = iteration + 1
        return loss

    def calculate_raw(self, recs: List[TspLibRec]) -> float:
        edges = zip(recs[0:-1], recs[1:])
        loss = 0.0
        iteration = 1
        for edge in edges:
            d = self.edge_distance(edge)
            loss = loss + d
            iteration = iteration + 1

        fedge = self.final_edge_distance(recs)
        return loss + fedge

    def calculate(self, recs: List[TspLibRec]) -> float:
        edges = zip(recs[0:-1], recs[1:])
        penalty = 1.0
        loss = 0.0
        iteration = 1
        for edge in edges:
            d = self.edge_distance(edge)
            loss = loss + (penalty * d)

            if iteration % 10 == 0:
                penalty = self.calc_penalty(edge[1])
            iteration = iteration + 1

        fedge = self.final_edge_distance(recs)
        return loss + (penalty * fedge)

    def final_edge_distance(self, recs: List[TspLibRec]) -> float:
        tpl = (recs[-1], recs[0])
        return self.edge_distance(tpl)

    def edge_distance(self, edge: Tuple[TspLibRec, TspLibRec]) -> float:
        p1 = T.Point(edge[0].x, edge[0].y)
        p2 = T.Point(edge[1].x, edge[1].y)
        return Dist.euclidean(p1, p2)
