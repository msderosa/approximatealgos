from typing import Iterable, Tuple, List


# default join on the first member of both recs
def join(rs1: Iterable[Tuple[int, float]],
         rs2: Iterable[Tuple[int, float]]) -> List[Tuple[int, float, float]]:
    ts1 = list(rs1)
    ts2 = list(rs2)
    ls = [(a[0], a[1], b[1]) for a in ts1 for b in ts2 if a[0] == b[0]]
    return ls
