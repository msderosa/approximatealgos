import tsp.rw.rw_types as T
from typing import Set, Iterable


def subtract(rs1: Set[T.TspLibRec],
             rs2: Iterable[T.TspLibRec]) -> Set[T.TspLibRec]:
    return rs1.difference(rs2)
