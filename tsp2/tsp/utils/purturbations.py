from tsp.rw.rw_types import TspLibRec
from typing import List


def neighbor_swap(group: List[TspLibRec]) -> List[List[TspLibRec]]:
    rest = _neighbor_swap(group[1:])
    for r in rest:
        r.insert(0, group[0])
    return rest


def _neighbor_swap(group: List[TspLibRec]) -> List[List[TspLibRec]]:
    sz = len(group)
    if sz <= 2:
        return [group]
    else:
        hds = [
            [group[0], group[1]],
            [group[1], group[2]]
            ]
        tls = _neighbor_swap(group[2:])
        return [hd + tl for hd in hds for tl in tls]
