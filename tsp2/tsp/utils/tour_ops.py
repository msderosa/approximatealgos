from tsp.rw.rw_types import TspLibRec
from typing import List, Mapping, Dict


def rewrite_tour_start(start_node: int, xs: List[int]) -> List[int]:
    idx = xs.index(start_node)
    ys = xs[idx:] + xs[0:idx]
    assert len(xs) == len(ys), 'lengths match after rewrite'
    return ys


def order_nodes_by_tour(tour: List[int],
                        src_data: List[TspLibRec]) -> List[TspLibRec]:
    assert len(tour) == len(src_data), 'unable to order nodes, length mismatch'

    n_to_node = _map_src_data(src_data)
    ordered = []
    for num in tour:
        rec = n_to_node[num]
        if rec.node == 197769:
            rec = TspLibRec(node=0, x=rec.x, y=rec.y)
        ordered.append(rec)
    return ordered


def _map_src_data(src_data: List[TspLibRec]) -> Mapping[int, TspLibRec]:
    m = {}
    for item in src_data:
        m[item.node] = item
    return m


def normed_to_true_mapping(normed: List[TspLibRec],
                           tru: List[TspLibRec]) -> Dict[int, int]:
    assert len(normed) == len(tru), "can not build a total mapping"
    temp = {}
    rng = range(0, len(normed))
    for idx in rng:
        norm_el = normed[idx]
        true_el = tru[idx]
        assert norm_el.x == true_el.x and norm_el.y == true_el.y, \
            "expecting identical location nodes (equal)"
        temp[norm_el.node] = true_el.node
    return temp
