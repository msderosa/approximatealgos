#! /usr/bin/env python3
import traceback
import tsp.rw.tsplib as R
import tsp.utils.tour_ops as Tops
import tsp.utils.loss as L
from tsp.tactics.tenblock_list import TenBlockList


def main():
    try:
        rdr = R.TspLib()
        data_in = rdr.read("docs/cities.tsp")
        tour = rdr.read_tour("docs/cities_1498187.out")
        tour2 = Tops.rewrite_tour_start(197769, tour)
        assert tour2[0] == 197769, 'unexpected start of tour'

        tour_ordered_data = Tops.order_nodes_by_tour(tour2, data_in[1])
        loss = L.Loss()
        score_raw = loss.calculate_raw(tour_ordered_data)
        print("score raw: ", score_raw)
        score = loss.calculate(tour_ordered_data)
        print("score: ", score)

        test_adj_prime_swap(loss, tour_ordered_data)

        print("success")
    except Exception:
        traceback.print_exc()


def test_adj_prime_swap(loss, tour_ordered_data) -> None:
    blk_list = TenBlockList(loss, tour_ordered_data)
    blk_list.optimize()
    score = loss.calculate(blk_list._opt_tour)
    print("score opt #1", score)


if __name__ == "__main__":
    main()
