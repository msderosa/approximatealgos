#! /usr/bin/env python3
from tsp.rw.rw_types import TspLibRec
from typing import List, Set
import traceback
import tsp.rw.tsplib as R
import tsp.tactics.insert_prime as Tac
import tsp.utils.loss as L
import tsp.utils.tour_ops as Tops


def main():
    try:
        non_prime_circuit = read_np_circuit()
        primes = read_prime_data()
        target_len = len(non_prime_circuit) + len(primes)

        loss = L.Loss()
        tac = Tac.InsertPrime(loss, non_prime_circuit, primes)
        tac.prime_backlog(set())
        augmented_tour = tac.optimize()
        print("len augmented tour: ", len(augmented_tour))
        print("target length: ", target_len)
        assert_no_duplicates(augmented_tour)

        acc_default_backlog = set()
        while len(augmented_tour) < target_len:
            var_adb = len(acc_default_backlog)
            uninserted_primes = primes.difference(augmented_tour)
            below, above = tac.split_by_avg_distance_to_base_circuit(
                uninserted_primes)
            acc_default_backlog = acc_default_backlog.union(above)
            try_insert_primes = primes.difference(acc_default_backlog)

            tac = Tac.InsertPrime(loss,
                                  non_prime_circuit,
                                  try_insert_primes)
            tac.prime_backlog(acc_default_backlog)
            augmented_tour = tac.optimize()
            print("len augmented tour: ", len(augmented_tour))
            print("target length: ", target_len)
            assert_no_duplicates(augmented_tour)

        score_raw = loss.calculate_raw(augmented_tour)
        print("score raw: ", score_raw)
        score = loss.calculate(augmented_tour)
        print("score: ", score)

        print("success")
    except Exception:
        traceback.print_exc()


def assert_no_duplicates(augmented_tour: List[TspLibRec]) -> None:
    tmp = set([a.node for a in augmented_tour])
    assert len(tmp) == len(augmented_tour), \
        "duplicate check does not pass: ".format(len(tmp))


def read_prime_data() -> Set[TspLibRec]:
    rdr = R.TspLib()
    prime_data = rdr.read("docs/cities_prime.tsp")
    return set(prime_data[1])


def read_np_circuit() -> List[TspLibRec]:
    rdr = R.TspLib()
    normed_data = rdr.read("docs/cities_not_prime_normed.tsp")
    tour = rdr.read_tour("docs/cities_not_prime_normed1386105.out")
    tour_ordered_data = Tops.order_nodes_by_tour(tour, normed_data[1])

    unnormed_data = rdr.read("docs/cities_not_prime.tsp")
    normed_to_true = Tops.normed_to_true_mapping(
        normed_data[1], unnormed_data[1])

    return [TspLibRec(node=normed_to_true[rec.node],
                      x=rec.x,
                      y=rec.y) for rec in tour_ordered_data]


if __name__ == "__main__":
    main()
