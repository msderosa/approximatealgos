
try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

config = {
    'description': 'calulates diffs from two revisions of the nyc dot sign files',
    'version': '0.1',
    'install_requires': ['nose'],
    'packages': ['diff'],
    'scripts': ['updatec.py'],
    'name': 'nycupdate'
}

setup(**config)
