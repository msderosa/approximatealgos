from tsp.tactics.mod_tenblock import ModTenBlock, Direction
import tests.factories.tsplibrec_factory as F
from tsp.rw.rw_types import TspLibRec
from tsp.utils.loss import Loss
import unittest


class ModTenBlockTests(unittest.TestCase):
    loss: Loss

    @classmethod
    def setUpClass(self) -> None:
        self.loss = Loss()

    def test_interval_scan_primes_lower(self) -> None:
        recs = F.mk_test_data()
        blk = ModTenBlock(recs[1:], None, self.loss)

        xs = blk.interval_scan_primes(8)
        self.assertEqual(1, len(xs))
        a = xs[0]
        self.assertEqual(18, a)

    def test_interval_scan_primes_upper(self) -> None:
        recs = F.mk_test_data()
        blk = ModTenBlock(recs[1:], None, self.loss)

        xs = blk.interval_scan_primes(10)
        self.assertEqual(1, len(xs))
        a = xs[0]
        self.assertEqual(10, a)

    def test_swap(self) -> None:
        recs = F.mk_test_data()
        blk = ModTenBlock(recs[1:], None, self.loss)
        blk.swap_D(recs, 2, 3)
        self.assertEquals(3, recs[2].node)
        self.assertEquals(2, recs[3].node)

    def test_split_up(self) -> None:
        recs = F.mk_test_data()
        blk = ModTenBlock(recs[1:], None, self.loss)
        blk.set_prev_blk_terminal(recs[0])
        a = blk.optimize_swap(Direction.Up)
        self.assertIsNone(a)

    def test_split_down(self) -> None:
        recs = F.mk_test_data()
        recs[11] = TspLibRec(node=11, x=9.9, y=9.9)
        blk = ModTenBlock(recs[1:], None, self.loss)
        blk.set_prev_blk_terminal(recs[0])
        a = blk.optimize_swap(Direction.Down)
        self.assertIsNotNone(a)
        if a is not None:
            self.assertEqual(2, len(a))
            self.assertEqual(11, a[0].terminal().node)

    def test_bug1(self) -> None:
        recs = F.mk_test_data_bug1()
        blk = ModTenBlock(recs, None, self.loss)
        blk.set_prev_blk_terminal(
            TspLibRec(node=19031, x=4389.92607486091, y=3386.95974437391)
            )
        a = blk.optimize_swap(Direction.Up)
        self.assertIsNotNone(a)
        if a is not None:
            self.assertEqual(1, len(a))

    def test_random_start_node(self) -> None:
        recs = F.mk_test_data_bug1()
        blk = ModTenBlock(recs, None, self.loss)
        a = blk.random_start_node()
        sz = blk.size()
        self.assertTrue(a >= 0, "was: {}".format(a))
        self.assertTrue(a <= sz - 3, "was: {} in block len: {}".format(
            a, blk.size()))

    def test_logical_2l_ranges(self) -> None:
        recs = F.mk_test_data()
        blk = ModTenBlock(recs[1:], None, self.loss)
        blk.set_prev_blk_terminal(recs[0])

        rngs1 = blk.logical_2l_ranges()
        rngs2 = blk.filter_logical_2l_ranges(rngs1)
        self.assertEqual(0, len(rngs2))

    def test_get_by_logical_pos(self) -> None:
        recs = F.mk_test_data()
        blk = ModTenBlock(recs[1:], None, self.loss)
        blk.set_prev_blk_terminal(recs[0])

        aa = blk.get_by_logical_pos(0)
        self.assertEqual(0, aa.node)
        ab = blk.get_by_logical_pos(20)
        self.assertEqual(20, ab.node)

    def test_permute_by_logical_2l_range(self) -> None:
        recs = F.mk_test_data10()
        blk = ModTenBlock(recs[1:], None, self.loss)
        blk.set_prev_blk_terminal(recs[0])

        recs = blk.permute_by_logical_2l_range((1, 5))
        self.assertEqual(5, recs[0].node)
        self.assertEqual(4, recs[1].node)
        self.assertEqual(3, recs[2].node)
        self.assertEqual(2, recs[3].node)
        self.assertEqual(1, recs[4].node)
        self.assertEqual(6, recs[5].node)
        self.assertEqual(7, recs[6].node)

    def test_lk_opt_no_optimization_possible(self) -> None:
        recs = F.mk_test_data_primeend()
        blk = ModTenBlock(recs[1:], None, self.loss)
        blk.set_prev_blk_terminal(recs[0])
        a = blk.optimize_lin_ker()
        self.assertIsNone(a)

    def test_lk_opt_one_optimization(self) -> None:
        recs = F.mk_test_data_primeend()
        temp = recs[6]
        recs[6] = recs[7]
        recs[7] = temp
        blk = ModTenBlock(recs[1:], None, self.loss)
        blk.set_prev_blk_terminal(recs[0])
        a = blk.optimize_lin_ker()
        self.assertIsNotNone(a)

