import tsp.tactics.mod_tenblock_helper as H
import tests.factories.tsplibrec_factory as F
from tsp.utils.loss import Loss
import unittest


class ModTenBlockHelperTests(unittest.TestCase):
    loss: Loss

    @classmethod
    def setUpClass(self) -> None:
        self.loss = Loss()

    def test_chunk_next(self) -> None:
        recs = F.mk_test_data()
        h = H.ModTenBlockHelper(self.loss)
        ahd, atl = h.chunk_next(recs[1:])
        self.assertEqual([], atl)
        self.assertEqual(recs[-1], ahd[-1])

    def test_bug1(self) -> None:
        recs = F.mk_test_data_bug1()
        h = H.ModTenBlockHelper(self.loss)
        ahd, atl = h.chunk_next(recs)
        self.assertEqual([], atl)
        self.assertEqual(recs[-1], ahd[-1])
