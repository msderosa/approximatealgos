from tsp.tactics.tenblock_list import TenBlockList
from typing import List
from tsp.rw.rw_types import TspLibRec
from tsp.utils.loss import Loss
import unittest


class TenBlockListTests(unittest.TestCase):
    loss: Loss

    @classmethod
    def setUpClass(self):
        self.loss = Loss()

    def test_block_unblock(self) -> None:
        tour = self.mk_test_data()
        bs = TenBlockList(self.loss, tour)
        self.assertIsNotNone(bs)

        bls = TenBlockList(self.loss, tour)
        blks = bls.block()
        self.assertEqual(2, len(blks))
        for blk in blks:
            (c, p) = blk.get_unoptimized_values()
            self.assertEqual(0, p[0].node % 10)
            self.assertEqual(0, p[10].node % 10)
            self.assertEqual(0, c[0].node % 10)

        tour2 = bls.unblock(blks)
        for idx in range(0, len(tour)):
            self.assertEqual(tour[idx].node, tour2[idx].node)

    def test_optimize(self) -> None:
        tour = self.mk_test_data()
        bs = TenBlockList(self.loss, tour)
        bs.optimize()

        tour2 = bs._opt_tour
        for idx in range(0, len(tour)):
            self.assertEqual(tour[idx].node, tour2[idx].node)

    def mk_test_data(self) -> List[TspLibRec]:
        return [
            TspLibRec(node=0, x=0, y=0),
            TspLibRec(node=1, x=1, y=1),
            TspLibRec(node=2, x=2, y=2),
            TspLibRec(node=3, x=3, y=3),
            TspLibRec(node=4, x=4, y=4),
            TspLibRec(node=5, x=5, y=5),
            TspLibRec(node=6, x=6, y=6),
            TspLibRec(node=7, x=7, y=7),
            TspLibRec(node=8, x=8, y=8),
            TspLibRec(node=9, x=9, y=9),
            TspLibRec(node=10, x=10, y=10),
            TspLibRec(node=11, x=11, y=11),
            TspLibRec(node=12, x=12, y=12),
            TspLibRec(node=13, x=13, y=13),
            TspLibRec(node=14, x=14, y=14),
            TspLibRec(node=15, x=15, y=15),
            TspLibRec(node=16, x=16, y=16),
            TspLibRec(node=17, x=17, y=17),
            TspLibRec(node=18, x=18, y=18),
            TspLibRec(node=19, x=19, y=19),
            TspLibRec(node=20, x=20, y=20),
            TspLibRec(node=21, x=21, y=21),
            TspLibRec(node=22, x=22, y=22),
            ]
