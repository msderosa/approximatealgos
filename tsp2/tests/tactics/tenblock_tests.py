from tsp.tactics.tenblock import TenBlock
from typing import List, Tuple
from tsp.rw.rw_types import TspLibRec
from tsp.utils.loss import Loss
import unittest


class TenBlockTests(unittest.TestCase):
    loss: Loss

    @classmethod
    def setUpClass(self):
        self.loss = Loss()

    def test_create(self) -> None:
        (curr, prev) = self.mk_test_data()
        blk = TenBlock(self.loss, curr, prev)
        self.assertIsNotNone(blk)
        a = blk.score(prev)
        self.assertTrue(a - 14.14 < 0.1)

    def test_swap_right(self) -> None:
        (curr, prev) = self.mk_test_data()
        blk = TenBlock(self.loss, curr, prev)
        (c, p) = blk.swap_right(1)
        self.assertEqual(11, c[0].node)
        self.assertEqual(10, c[1].node)
        self.assertEqual(11, p[-1].node)

    def test_swap_left(self) -> None:
        (curr, prev) = self.mk_test_data()
        blk = TenBlock(self.loss, curr, prev)
        (c, p) = blk.swap_left(7)
        self.assertEqual(7, p[-1].node)
        self.assertEqual(7, c[0].node)

    def test_index_first_prime(self) -> None:
        (curr, prev) = self.mk_test_data()
        blk = TenBlock(self.loss, curr, prev)
        a1 = blk.index_first_prime_right()
        a2 = blk.index_first_prime_left()
        self.assertEqual(1, a1)
        self.assertEqual(7, a2)

    def test_optimize(self) -> None:
        (curr, prev) = self.mk_test_data()
        blk = TenBlock(self.loss, curr, prev)
        blk.optimize()
        (a1, a2) = blk.get_optimized_values()
        self.assertIsNone(a1)
        self.assertIsNone(a2)

    def mk_test_data(self) -> Tuple[List[TspLibRec], List[TspLibRec]]:
        curr = [
            TspLibRec(node=10, x=10, y=10),
            TspLibRec(node=11, x=11, y=11),
            TspLibRec(node=12, x=12, y=12),
            TspLibRec(node=13, x=13, y=13),
            TspLibRec(node=14, x=14, y=14),
            TspLibRec(node=15, x=15, y=15),
            TspLibRec(node=16, x=16, y=16),
            TspLibRec(node=17, x=17, y=17),
            TspLibRec(node=18, x=18, y=18),
            TspLibRec(node=19, x=19, y=19),
            TspLibRec(node=20, x=20, y=20),
            ]
        prev = [
            TspLibRec(node=0, x=0, y=0),
            TspLibRec(node=1, x=1, y=1),
            TspLibRec(node=2, x=2, y=2),
            TspLibRec(node=3, x=3, y=3),
            TspLibRec(node=4, x=4, y=4),
            TspLibRec(node=5, x=5, y=5),
            TspLibRec(node=6, x=6, y=6),
            TspLibRec(node=7, x=7, y=7),
            TspLibRec(node=8, x=8, y=8),
            TspLibRec(node=9, x=9, y=9),
            TspLibRec(node=10, x=10, y=10),
            ]
        return (curr, prev)
