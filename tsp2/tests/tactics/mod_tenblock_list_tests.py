from tsp.tactics.mod_tenblock_list import ModTenBlockList
from typing import List
from tsp.rw.rw_types import TspLibRec
from tsp.utils.loss import Loss
import unittest


class ModTenBlockListTests(unittest.TestCase):
    loss: Loss

    @classmethod
    def setUpClass(self):
        self.loss = Loss()

    def test_create(self) -> None:
        tour = self.mk_test_data()
        bs = ModTenBlockList(tour, self.loss)
        self.assertIsNotNone(bs)
        self.assertEqual(1, bs.size())

        a = bs.at(0)
        self.assertEqual(0, a.initial().node)

    def test_unblock(self) -> None:
        tour = self.mk_test_data()
        bs = ModTenBlockList(tour, self.loss)
        a = bs.unblock()
        self.assertEqual(len(tour), len(a))
        self.assertEqual(tour[5], a[5])
        self.assertEqual(tour[10], a[10])

    def mk_test_data(self) -> List[TspLibRec]:
        return [
            TspLibRec(node=0, x=0, y=0),
            TspLibRec(node=1, x=1, y=1),
            TspLibRec(node=2, x=2, y=2),
            TspLibRec(node=3, x=3, y=3),
            TspLibRec(node=4, x=4, y=4),
            TspLibRec(node=5, x=5, y=5),
            TspLibRec(node=6, x=6, y=6),
            TspLibRec(node=7, x=7, y=7),
            TspLibRec(node=8, x=8, y=8),
            TspLibRec(node=9, x=9, y=9),
            TspLibRec(node=10, x=10, y=10),
            TspLibRec(node=11, x=11, y=11),
            TspLibRec(node=12, x=12, y=12),
            TspLibRec(node=13, x=13, y=13),
            TspLibRec(node=14, x=14, y=14),
            TspLibRec(node=15, x=15, y=15),
            TspLibRec(node=16, x=16, y=16),
            TspLibRec(node=17, x=17, y=17),
            TspLibRec(node=18, x=18, y=18),
            TspLibRec(node=19, x=19, y=19),
            TspLibRec(node=20, x=20, y=20),
            TspLibRec(node=21, x=21, y=21),
            TspLibRec(node=22, x=22, y=22)
            ]
