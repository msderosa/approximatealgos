import tsp.tactics.indexed_node_set as Tac
from tsp.rw.rw_types import TspLibRec
import unittest


class IndexedNodeSetTests(unittest.TestCase):

    def test_n_nearest_to_both(self) -> None:
        ns = [
            TspLibRec(node=129, x=0.0, y=0.0),
            TspLibRec(node=12, x=1.0, y=0.0),
            TspLibRec(node=372, x=1.0, y=1.0),
            TspLibRec(node=47, x=0.0, y=1.0)]
        tac = Tac.IndexedNodeSet(ns)
        ys = tac.n_nearest_to_both(
            TspLibRec(node=1111, x=1.0, y=1.1),
            TspLibRec(node=1112, x=1.0, y=1.2),
            1)
        self.assertEqual(1, len(ys))
        zs = tac.n_nearest_to_both(
            TspLibRec(node=1111, x=1.0, y=1.1),
            TspLibRec(node=1112, x=1.0, y=1.2),
            2)
        self.assertEqual(2, len(zs))
        nos = [rec.node for (rec, d) in zs]
        self.assertTrue(372 in nos)
        self.assertTrue(47 in nos)

    def test_nearest(self) -> None:
        ns = [
            TspLibRec(node=129, x=0.0, y=0.0),
            TspLibRec(node=12, x=1.0, y=0.0),
            TspLibRec(node=372, x=1.0, y=1.0),
            TspLibRec(node=47, x=0.0, y=1.0)]
        tac = Tac.IndexedNodeSet(ns)
        actual_rec, actual_dist = tac.nearest(
            TspLibRec(node=2344, x=1.2, y=1.32))
        self.assertEqual(372, actual_rec.node)
        self.assertTrue(actual_dist > 0)

    def test_second_nearest(self) -> None:
        ns = [
            TspLibRec(node=129, x=0.0, y=0.0),
            TspLibRec(node=12, x=1.0, y=0.0),
            TspLibRec(node=372, x=1.0, y=1.0),
            TspLibRec(node=47, x=0.0, y=1.0)]
        tac = Tac.IndexedNodeSet(ns)
        actual_rec, actual_dist = tac.second_nearest(
            TspLibRec(node=372, x=1.0, y=1.0))
        self.assertEqual(12, actual_rec.node)
        self.assertTrue(actual_dist > 0)

    def test_min_distance(self) -> None:
        non_primes = [
            TspLibRec(node=1, x=5.1, y=5.2),
            TspLibRec(node=4, x=6.1, y=6.2)]
        tac = Tac.IndexedNodeSet(non_primes)
        distances = [
            (0, 0.234, 0.123),
            (1, 1.455, 1.236),
            (2, 2.344, 2.087)
            ]
        actual = tac.min_distance(distances)
        self.assertEqual(distances[0], actual)

    def test_neighbors(self) -> None:
        ns = [
            TspLibRec(node=129, x=0.0, y=0.0),
            TspLibRec(node=12, x=1.0, y=0.0),
            TspLibRec(node=372, x=1.0, y=1.0),
            TspLibRec(node=47, x=0.0, y=1.0)]
        tac = Tac.IndexedNodeSet(ns)
        a_low = tac.neighbor_low(TspLibRec(node=372, x=1.0, y=1.0))
        self.assertEqual(12, a_low.node)
        a_high = tac.neighbor_high(TspLibRec(node=372, x=1.0, y=1.0))
        self.assertEqual(47, a_high.node)

    def test_n_nearest(self) -> None:
        ns = [
            TspLibRec(node=129, x=0.0, y=0.0),
            TspLibRec(node=12, x=1.0, y=0.0),
            TspLibRec(node=372, x=1.0, y=1.0),
            TspLibRec(node=47, x=0.0, y=1.0)]
        tac = Tac.IndexedNodeSet(ns)
        acts = tac.n_nearest(TspLibRec(node=222, x=1.0, y=1.1), 3)
        self.assertEqual(3, len(acts))
        tpl_fst = acts[0]
        self.assertEqual(372, tpl_fst[0].node)
        tpl_lst = acts[2]
        self.assertEqual(12, tpl_lst[0].node)
        d = 0.0
        for a in acts:
            self.assertTrue(d < a[1])
            d = a[1]

    def test_n_nearest2(self) -> None:
        ns = [
            TspLibRec(node=129, x=0.0, y=0.0),
            TspLibRec(node=12, x=1.0, y=0.0),
            TspLibRec(node=372, x=1.0, y=1.0),
            TspLibRec(node=47, x=0.0, y=1.0)]
        tac = Tac.IndexedNodeSet(ns)
        acts = tac.n_nearest(TspLibRec(node=222, x=0.0, y=0.1), 3)
        self.assertEqual(3, len(acts))
        tpl_fst = acts[0]
        self.assertEqual(129, tpl_fst[0].node)
        tpl_lst = acts[2]
        self.assertEqual(12, tpl_lst[0].node)
        d = 0.0
        for a in acts:
            self.assertTrue(d < a[1])
            d = a[1]

    def test_rebuild(self) -> None:
        ns = [
            TspLibRec(node=129, x=0.0, y=0.0),
            TspLibRec(node=12, x=1.0, y=0.0),
            TspLibRec(node=372, x=1.0, y=1.0),
            TspLibRec(node=47, x=0.0, y=1.0)]
        tac = Tac.IndexedNodeSet(ns)
        self.assertEqual(4, tac.size())
        tac.mark_as_used(TspLibRec(node=129, x=0.0, y=0.0))
        tac._rebuild()
        self.assertEqual(3, tac.size())

    def test_nearest_unused(self) -> None:
        ns = [
            TspLibRec(node=129, x=0.0, y=0.0),
            TspLibRec(node=12, x=1.0, y=0.0),
            TspLibRec(node=372, x=1.0, y=1.0),
            TspLibRec(node=47, x=0.0, y=1.0)]
        tac = Tac.IndexedNodeSet(ns)
        self.assertEqual(4, tac.size())
        tac.mark_as_used(TspLibRec(node=129, x=0.0, y=0.0))

        r, d = tac.nearest_unused(TspLibRec(node=1291, x=0.2, y=0.0))
        self.assertEqual(12, r.node)
