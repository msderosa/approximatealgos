from tsp.utils.loss import Loss
# from tsp.rw.rw_types import TspLibRec
# import tsp.tactics.insert_prime as Tac
import unittest


class InsertPrimeTests(unittest.TestCase):
    loss: Loss

    @classmethod
    def setUpClass(self):
        self.loss = Loss()

    def test_nothing(self):
        self.assertEqual(1, 1)
