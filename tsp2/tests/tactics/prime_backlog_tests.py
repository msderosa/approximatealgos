import tsp.tactics.prime_backlog as Tac
from tsp.rw.rw_types import TspLibRec
from tsp.tactics.tactic_types import MinInsertCost
import unittest


class PrimeBacklogTests(unittest.TestCase):

    def test_create(self) -> None:
        q = Tac.PrimeBacklog()
        self.assertIsNotNone(q)

    def test_get_waiting_inserts_none(self) -> None:
        b = Tac.PrimeBacklog()
        b.add(
            TspLibRec(node=8766, x=23.3, y=977.33),
            MinInsertCost(
                low_node=TspLibRec(node=34, x=28.1, y=7.4),
                low_cost=87.0,
                high_node=TspLibRec(node=3443, x=98.21, y=87.4),
                high_cost=35.0)
            )
        chunk = [
            TspLibRec(node=65, x=76.4, y=35.2)
            ]
        actual = b.get_waiting_inserts(chunk)
        self.assertEqual([], actual)

    def test_get_waiting_inserts_one(self) -> None:
        b = Tac.PrimeBacklog()
        b.add(
            TspLibRec(node=8766, x=23.3, y=977.33),
            MinInsertCost(
                low_node=TspLibRec(node=34, x=28.1, y=7.4),
                low_cost=87.0,
                high_node=TspLibRec(node=3443, x=98.21, y=87.4),
                high_cost=35.0)
            )
        chunk = [
            TspLibRec(node=65, x=76.4, y=35.2),
            TspLibRec(node=3443, x=98.21, y=87.4)
            ]
        actual = b.get_waiting_inserts(chunk)
        self.assertEqual(1, len(actual))

    def test_delete_noop(self) -> None:
        bklog = Tac.PrimeBacklog()
        bklog.delete(TspLibRec(node=8766, x=23.3, y=977.33))
        self.assertTrue(True, 'we should get here')

    def test_delete_existing(self) -> None:
        b = Tac.PrimeBacklog()
        b.add(
            TspLibRec(node=8766, x=23.3, y=977.33),
            MinInsertCost(
                low_node=TspLibRec(node=34, x=28.1, y=7.4),
                low_cost=87.0,
                high_node=TspLibRec(node=3443, x=98.21, y=87.4),
                high_cost=35.0)
            )
        self.assertEqual(1, b.size())
        b.delete(TspLibRec(node=8766, x=23.3, y=977.33))
        self.assertEqual(0, b.size())

    def test_add_same_tour_insert_pos(self) -> None:
        bklg = Tac.PrimeBacklog()
        bklg.add(
            TspLibRec(node=8766, x=23.3, y=977.33),
            MinInsertCost(
                low_node=TspLibRec(node=34, x=28.1, y=7.4),
                low_cost=87.0,
                high_node=TspLibRec(node=3443, x=98.21, y=87.4),
                high_cost=35.0)
            )
        a1 = bklg.exists_at(TspLibRec(node=8766, x=23.3, y=977.33), 3443)
        self.assertTrue(a1)
        self.assertEqual(1, len(bklg._index_prime_to_pos))

        bklg.add(
            TspLibRec(node=8767, x=23.301, y=977.327),
            MinInsertCost(
                low_node=TspLibRec(node=34, x=28.1, y=7.4),
                low_cost=87.0,
                high_node=TspLibRec(node=3443, x=98.21, y=87.4),
                high_cost=35.0)
            )
        self.assertTrue(True, 'we should get here')
        a2 = bklg.exists_at(TspLibRec(node=8766, x=23.3, y=977.33), 3443)
        self.assertTrue(a2)
        a3 = bklg.exists_at(TspLibRec(node=8767, x=23.301, y=977.327), 3443)
        self.assertTrue(a3)
        self.assertEqual(2, len(bklg._index_prime_to_pos))

    def test_exists_and_deletes(self) -> None:
        bklg = Tac.PrimeBacklog()
        bklg.add(
            TspLibRec(node=8766, x=23.3, y=977.33),
            MinInsertCost(
                low_node=TspLibRec(node=34, x=28.1, y=7.4),
                low_cost=87.0,
                high_node=TspLibRec(node=3443, x=98.21, y=87.4),
                high_cost=35.0)
            )
        bklg.add(
            TspLibRec(node=8767, x=23.301, y=977.327),
            MinInsertCost(
                low_node=TspLibRec(node=34, x=28.1, y=7.4),
                low_cost=87.0,
                high_node=TspLibRec(node=3443, x=98.21, y=87.4),
                high_cost=35.0)
            )
        bklg.delete(TspLibRec(node=8766, x=23.3, y=977.33))
        a = bklg.exists_at(TspLibRec(node=8766, x=23.3, y=977.33), 3443)
        self.assertFalse(a)
        self.assertEquals(1, bklg.size())
