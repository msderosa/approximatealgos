import tsp.stats.summary_stats as Stats
import unittest


class SummaryStatsTests(unittest.TestCase):

    def test_median(self) -> None:
        ns = [5.0, 1.0, 2.0, 4.0, 3.0]
        actual = Stats.median(ns)
        self.assertEqual(3.0, actual)
