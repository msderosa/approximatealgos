from typing import List
from tsp.rw.rw_types import TspLibRec


def mk_test_data() -> List[TspLibRec]:
    return [
            TspLibRec(node=0, x=0, y=0),
            TspLibRec(node=1, x=1, y=1),
            TspLibRec(node=2, x=2, y=2),
            TspLibRec(node=3, x=3, y=3),
            TspLibRec(node=4, x=4, y=4),
            TspLibRec(node=5, x=5, y=5),
            TspLibRec(node=6, x=6, y=6),
            TspLibRec(node=7, x=7, y=7),
            TspLibRec(node=8, x=8, y=8),
            TspLibRec(node=9, x=9, y=9),
            TspLibRec(node=10, x=10, y=10),
            TspLibRec(node=11, x=11, y=11),
            TspLibRec(node=12, x=12, y=12),
            TspLibRec(node=13, x=13, y=13),
            TspLibRec(node=14, x=14, y=14),
            TspLibRec(node=15, x=15, y=15),
            TspLibRec(node=16, x=16, y=16),
            TspLibRec(node=17, x=17, y=17),
            TspLibRec(node=18, x=18, y=18),
            TspLibRec(node=19, x=19, y=19),
            TspLibRec(node=20, x=20, y=20)
        ]


def mk_test_data_primeend() -> List[TspLibRec]:
    return [
            TspLibRec(node=0, x=0, y=0),
            TspLibRec(node=1, x=1, y=1),
            TspLibRec(node=2, x=2, y=2),
            TspLibRec(node=3, x=3, y=3),
            TspLibRec(node=4, x=4, y=4),
            TspLibRec(node=5, x=5, y=5),
            TspLibRec(node=6, x=6, y=6),
            TspLibRec(node=7, x=7, y=7),
            TspLibRec(node=8, x=8, y=8),
            TspLibRec(node=9, x=9, y=9),
            TspLibRec(node=10, x=10, y=10),
            TspLibRec(node=11, x=11, y=11),
            TspLibRec(node=12, x=12, y=12),
            TspLibRec(node=13, x=13, y=13),
            TspLibRec(node=14, x=14, y=14),
            TspLibRec(node=15, x=15, y=15),
            TspLibRec(node=16, x=16, y=16),
            TspLibRec(node=17, x=17, y=17),
            TspLibRec(node=18, x=18, y=18),
            TspLibRec(node=19, x=19, y=19),
            TspLibRec(node=23, x=20, y=20)
        ]


def mk_test_data10() -> List[TspLibRec]:
    return [
            TspLibRec(node=0, x=0, y=0),
            TspLibRec(node=1, x=1, y=1),
            TspLibRec(node=2, x=2, y=2),
            TspLibRec(node=3, x=3, y=3),
            TspLibRec(node=4, x=4, y=4),
            TspLibRec(node=5, x=5, y=5),
            TspLibRec(node=6, x=6, y=6),
            TspLibRec(node=7, x=7, y=7),
            TspLibRec(node=8, x=8, y=8),
            TspLibRec(node=9, x=9, y=9),
            TspLibRec(node=10, x=10, y=10),
        ]


def mk_test_data_bug1() -> List[TspLibRec]:
    return [
            TspLibRec(node=51140, x=4395.87519726173, y=3389.73048205382),
            TspLibRec(node=78875, x=4394.66446022188, y=3395.85321197431),
            TspLibRec(node=46515, x=4403.28935904571, y=3395.86924616722),
            TspLibRec(node=92627, x=4407.11146322268, y=3391.79902403821),
            TspLibRec(node=125845, x=4411.93279234111, y=3396.72288210819),
            TspLibRec(node=17237, x=4415.15036647538, y=3392.55427486007),
            TspLibRec(node=23422, x=4412.23871375656, y=3388.10082249307),
            TspLibRec(node=73291, x=4406.22974335614, y=3386.49845350303),
            TspLibRec(node=61627, x=4401.15432961306, y=3388.11531392338),
            TspLibRec(node=79066, x=4399.34967225412, y=3382.95026055496),
            TspLibRec(node=107945, x=4394.53598630906, y=3381.80711769999),
            TspLibRec(node=109811, x=4393.37614102359, y=3376.25864504245),
            TspLibRec(node=126095, x=4388.19523754884, y=3374.62428460428),
            TspLibRec(node=42896, x=4388.34416336164, y=3379.87231502078),
            TspLibRec(node=89146, x=4383.97698648742, y=3383.87138340136),
            TspLibRec(node=101391, x=4379.32789730089, y=3387.25160585993),
            TspLibRec(node=68453, x=4373.71654914992, y=3384.38919419502),
            TspLibRec(node=76368, x=4376.62625773135, y=3378.78694327336),
            TspLibRec(node=148997, x=4383.12279801536, y=3377.12293682293),
            TspLibRec(node=75967, x=4379.85762041565, y=3374.24613119162)
        ]
