import tsp.rw.rw_types as T
import tsp.utils.loss as L
import unittest


class LossTests(unittest.TestCase):

    def test_calculate(self) -> None:
        score = L.Loss()
        data = [
            T.TspLibRec(node=0, x=0, y=0),
            T.TspLibRec(node=1, x=2, y=0),
            T.TspLibRec(node=2, x=2, y=2),
            T.TspLibRec(node=3, x=0, y=2)
            ]
        actual = score.calculate(data)
        self.assertEqual(8.0, actual)

    def test_calculate_w_loss(self) -> None:
        score = L.Loss()
        data = [
            T.TspLibRec(node=0, x=0, y=0),
            T.TspLibRec(node=1, x=1, y=0),
            T.TspLibRec(node=2, x=2, y=0),

            T.TspLibRec(node=3, x=3, y=0),
            T.TspLibRec(node=4, x=3, y=1),
            T.TspLibRec(node=5, x=3, y=2),

            T.TspLibRec(node=6, x=3, y=3),
            T.TspLibRec(node=7, x=2, y=3),
            T.TspLibRec(node=8, x=1, y=3),

            T.TspLibRec(node=9, x=0, y=3),
            T.TspLibRec(node=10, x=0, y=2),
            T.TspLibRec(node=11, x=0, y=1)
            ]
        actual = score.calculate(data)
        self.assertEqual(12.2, actual)
