import tsp.utils.lists as L
from tsp.rw.rw_types import TspLibRec
import unittest


class RelationsTests(unittest.TestCase):

    def test_subtract1(self) -> None:
        bs = set([
            TspLibRec(node=1, x=1.0, y=1.0)])
        cs = [
            TspLibRec(node=5, x=5.0, y=5.0)]
        actual = L.subtract(bs, cs)
        self.assertEqual(1, len(actual))

    def test_subtract2(self) -> None:
        bs = set([
            TspLibRec(node=1, x=1.0, y=1.0),
            TspLibRec(node=3, x=3.0, y=3.0),
            TspLibRec(node=5, x=5.0, y=5.0)])
        cs = [
            TspLibRec(node=5, x=5.0, y=5.0)]
        actual = L.subtract(bs, cs)
        self.assertEqual(2, len(actual))
