from typing import List
import tsp.rw.rw_types as T
import tsp.utils.purturbations as P
import unittest


class PurturbationsTests(unittest.TestCase):

    def assert_ends_unchanged(self, xs: List[T.TspLibRec],
                              yss: List[List[T.TspLibRec]]) -> None:
        for ys in yss:
            self.assertEquals(xs[0].node, ys[0].node)
            self.assertEquals(xs[-1].node, ys[-1].node)

    def assert_content_size(self, n: int,
                            yss: List[List[T.TspLibRec]]) -> None:
        for ys in yss:
            self.assertEquals(n, len(ys))

    def test_neighbor_swap_3(self) -> None:
        data = [
            T.TspLibRec(node=0, x=0, y=0),
            T.TspLibRec(node=1, x=1, y=0),
            T.TspLibRec(node=2, x=2, y=0)
            ]
        actual = P.neighbor_swap(data)
        self.assertEqual(1, len(actual))
        self.assert_content_size(len(data), actual)
        self.assert_ends_unchanged(data, actual)

    def test_neighbor_swap_4(self) -> None:
        data = [
            T.TspLibRec(node=0, x=0, y=0),
            T.TspLibRec(node=1, x=1, y=0),
            T.TspLibRec(node=2, x=2, y=0),
            T.TspLibRec(node=3, x=3, y=0),
            ]
        actual = P.neighbor_swap(data)
        self.assertEqual(2, len(actual))
        self.assert_content_size(len(data), actual)
        self.assert_ends_unchanged(data, actual)
