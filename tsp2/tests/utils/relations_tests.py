import tsp.utils.relations as R
import unittest
import numpy as np


class RelationsTests(unittest.TestCase):

    def test_join(self) -> None:
        bs = [(1, 2.1213)]
        cs = [(1, 4.2233), (2, 3.43342)]
        actual = R.join(bs, cs)
        self.assertEqual(1, len(actual))
        fst = actual[0]
        self.assertEqual((1, 2.1213, 4.2233), fst)

    def test_join_bug1(self) -> None:
        ns9 = [8162, 9998]
        ns10 = [3016, 8162]
        ds9 = [38.73250102, 46.12505746]
        ds10 = [36.23763659, 37.13622832]
        actual = R.join(zip(ns10, ds10), zip(ns9, ds9))
        self.assertEqual(1, len(actual))
        fst = actual[0]
        self.assertEqual((8162, 37.13622832, 38.73250102), fst)

    def test_join_bug2(self) -> None:
        ns9 = np.asarray([8162, 9998])
        ns10 = np.asarray([3016, 8162])
        ds9 = np.asarray([38.73250102, 46.12505746])
        ds10 = np.asarray([36.23763659, 37.13622832])
        actual = R.join(zip(ns10, ds10), zip(ns9, ds9))
        self.assertEqual(1, len(actual))
        fst = actual[0]
        self.assertEqual((8162, 37.13622832, 38.73250102), fst)
