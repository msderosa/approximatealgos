import tsp.utils.tour_ops as Tops
import unittest
from tsp.rw.rw_types import TspLibRec


class TourOpsTests(unittest.TestCase):

    def test_rewrite_tour_start(self) -> None:
        tour = [1, 3, 6, 7, 0, 9, 4]
        actual = Tops.rewrite_tour_start(0, tour)
        self.assertEqual(0, actual[0])
        self.assertEqual(7, actual[-1])

    def test_normed_to_true_mapping(self) -> None:
        n_data = [
            TspLibRec(node=1, x=1.11, y=1.11),
            TspLibRec(node=2, x=2.22, y=2.22)
            ]
        t_data = [
            TspLibRec(node=27, x=1.11, y=1.11),
            TspLibRec(node=32, x=2.22, y=2.22)
            ]
        actual = Tops.normed_to_true_mapping(n_data, t_data)
        self.assertEqual(27, actual[1])
        self.assertEqual(32, actual[2])
