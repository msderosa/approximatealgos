import unittest
import tsp.rw.kgl as R


class KglTests(unittest.TestCase):

    def test_extract_field(self):
        rec = ["1", "4377.40597216624", "336.602082171235"]
        rdr = R.Kgl()
        actual = rdr.extract_field(rec)
        self.assertEqual(1, actual[0])
        self.assertEqual(4377.40597216624, actual[1])
        self.assertEqual(336.602082171235, actual[2])

    def test_extract_fields(self):
        recs = [
           "CityId,X,Y",
           "0,316.836739061509,2202.34070733524",
           "1,4377.40597216624,336.602082171235"]
        rdr = R.Kgl()
        fs = rdr.extract_fields(recs)
        self.assertEqual(2, len(fs))
        last = fs[1]
        self.assertEqual(1, last[0])
