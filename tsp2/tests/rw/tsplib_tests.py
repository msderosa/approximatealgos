import unittest
import tsp.rw.tsplib as R


class TspLibTests(unittest.TestCase):

    def test_parse_header_line(self):
        ln = "EDGE_WEIGHT_TYPE : EUC_2D"
        rdr = R.TspLib()
        actual = rdr.parse_header_line(ln)
        self.assertEqual('EDGE_WEIGHT_TYPE', actual[0])
        self.assertEqual('EUC_2D', actual[1])

    def test_read_header(self):
        fake_hdl = [
            "NAME : cities",
            "COMMENT : comp problem",
            "TYPE : TSP",
            "DIMENSION : 197769",
            "EDGE_WEIGHT_TYPE : EUC_2D",
            "NODE_COORD_SECTION",
            "197769 316.836739061509 2202.34070733524"]
        rdr = R.TspLib()
        actual = rdr.read_header(fake_hdl)
        self.assertEqual('cities', actual.name)
        self.assertEqual(197769, actual.dimension)

    def test_parse_node_coord(self):
        ln = "11 2052.11650024281 578.293526079925"
        rdr = R.TspLib()
        actual = rdr.parse_node_coord(ln)
        self.assertEqual(11, actual.node)
        self.assertEqual(2052.11650024281, actual.x)

    def test_read_node_coord_section(self):
        lns = [
            "197766 4775.8898741018 3103.84622791918",
            "197767 2994.23095453079 1931.76434369354",
            "197768 1354.76477830356 3218.10062536298",
            "EOF"]
        rdr = R.TspLib()
        actual = rdr.read_node_coord_section(lns)
        self.assertEqual(3, len(actual))
        fst = actual[0]
        self.assertEqual(197766, fst.node)
