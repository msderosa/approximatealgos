import unittest
import tsp.geo.point as T


class GeoTypesTests(unittest.TestCase):

    def test_center_of_mass(self) -> None:
        ps = [
            T.Point(x=1.0, y=1.0),
            T.Point(x=2.0, y=2.0)
            ]
        actual = T.center_of_mass(ps)
        self.assertEqual(1.5, actual.x)
        self.assertEqual(1.5, actual.y)
