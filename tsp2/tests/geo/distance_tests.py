import unittest
import tsp.geo.point as T
import tsp.geo.distance as Dist
from hypothesis import given
import hypothesis.strategies as st


class DistanceTests(unittest.TestCase):

    def test_trivial(self) -> None:
        p1 = T.Point(x=0, y=0)
        p2 = T.Point(x=3, y=4)
        actual = Dist.euclidean(p1, p2)
        self.assertEqual(5.0, actual)

    @given(st.floats(min_value=-10.0, max_value=10.0),
           st.floats(min_value=-10.0, max_value=10.0),
           st.floats(min_value=-10.0, max_value=10.0),
           st.floats(min_value=-10.0, max_value=10.0))
    def test_triange_inequality(self, x1: float, y1: float, x2: float,
                                y2: float) -> None:
        p1 = T.Point(x1, y1)
        p2 = T.Point(x2, y2)
        actual = Dist.euclidean(p1, p2)
        self.assertTrue(actual >= 0.0)

        dx = abs(x1 - x2)
        dy = abs(y1 - y2)
        self.assertTrue(actual <= dx + dy)
