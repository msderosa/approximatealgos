#! /usr/bin/env python3
import traceback
from typing import Set
from tsp.rw.rw_types import TspLibRec
import tsp.rw.tsplib as R
import tsp.utils.tour_ops as Tops
import tsp.utils.loss as L
from tsp.tactics.mod_tenblock_list import ModTenBlockList
import tsp.stats.block_stats as St


def main():
    try:
        rdr = R.TspLib()
        data_in = rdr.read("docs/cities.tsp")
        tour = rdr.read_tour("docs/cities_1498187.out")
        tour2 = Tops.rewrite_tour_start(197769, tour)
        assert tour2[0] == 197769, 'unexpected start of tour'

        tour_ordered_data = Tops.order_nodes_by_tour(tour2, data_in[1])
        loss = L.Loss()
        score_raw = loss.calculate_raw(tour_ordered_data)
        print("score raw: ", score_raw)
        score = loss.calculate(tour_ordered_data)
        print("score: ", score)

        print("creating blocks")
        blks = ModTenBlockList(tour_ordered_data, loss)
        print("initial number of blocks: {}".format(blks.size()))
        blks.optimize()
        print("final number of blocks: {}".format(blks.size()))

        tour2 = blks.unblock()
        score = loss.calculate(tour2)
        print("score: ", score)

        primes = read_prime_data()
        St.run_stats(blks._blks, primes)

        print("success")
    except Exception:
        traceback.print_exc()


def read_prime_data() -> Set[TspLibRec]:
    rdr = R.TspLib()
    prime_data = rdr.read("docs/cities_prime.tsp")
    return set(prime_data[1])


if __name__ == "__main__":
    main()
