# Overview
This is just a collection of miscellaneous code that I want to save for 
future reference. The sub-directory contents are as listed below.

# binpacking/
Description: playing with approximation algos - bin packing problems

Tags: Python, Julia, Approximation Algorithms

# tsp2/
Description: playing with approximation algos - traveling salesman problem

Tags: Python, Approximation Algorithms


