
container_l = 1000
container_w = 1000

target_xy_multipliers = [5, 10, 15, 25, 50, 100, 150, 200, 250]
fuzzy_xy_multipliers = {
    5: list(range(int(250/(250/5+1)), 5)),
    10: list(range(int(250/(250/10+1)), 10)),
    15: list(range(int(250/(250/15+1)), 15)),
    25: list(range(int(250/(250/25+1)), 25)),
    50: list(range(int(250/(250/50+1)), 50)),
    100: list(range(int(250/(250/100+1)), 100)),
    150: list(range(int(250/(250/150+1)), 150)),
    200: list(range(int(250/(250/200+1)), 200)),
    250: list(range(int(250/(250/250+1)), 250)),
}

target_z_multipliers = [5]
