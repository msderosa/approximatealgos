
import pdb 
import comp_geo.hilbert_sort as H

# take :: [Box] -> Integer -> [Box]
# returns a selection of boxes such that the sume
# of the z-face area is <= area
def take(bs, area_limit):
    ls = []
    acc_area = 0
    for b in bs:
        if acc_area + b.area_z() <= area_limit:
            ls.append(b)
            acc_area = acc_area + b.area_z()
        else:
            break
    return (ls, acc_area)

def hilbert_sort(dmin, dmax, ls):
    hs = H.HilbertSort((dmin, dmax), (dmin, dmax))
    ls.sort(key=hs.sort)
    return ls

