
import ops.lists as Lists
import config as Cfg

# max_dimensions :: [Box] -> (Integer, Integer, Integer)
def max_dimensions(bs):
    max_l = max_w = max_h = -1
    for b in bs:
        if b.x > max_l: max_l = b.x
        if b.y > max_w: max_w = b.y 
        if b.z > max_h: max_h = b.z 
    return (max_l, max_w, max_h)

# max_height :: [Box] -> (Integer, Integer)
def max_height(bs):
    pos = -1
    maximum = -1
    for i, b in enumerate(bs):
        if b.z > maximum:
            pos = i
            maximum = b.z
    return (pos, maximum)

def extract_layer_face_sample(bs):
    target_area = Cfg.container_l * Cfg.container_w
    ls, face_area = Lists.take(bs, target_area)
    pos, maximum = max_height(ls)
    return (ls, {
            'face_area': face_area,
            'z_max': maximum,
            'z_max_pos': pos
            })

