
import data.box as B
import policies.box_orientation as Bo

def parse():
    bs = []
    with open('./presents.csv') as file:
        for line in file:
            if line[0].isdigit():
                els = line.split(',')
                assert len(els) == 4, "incorrect dimensions extracted"
                box = B.Box(int(els[0]), 
                            int(els[1]), int(els[2]), int(els[3]),
                            orientation=Bo.x_decreasingD)
                bs.append(box)
    return bs


# notes
# 2 - 10
# 5 - 70
# 65 - 250
