require("algos/space_set")
require("data/box.jl")

module Container
using Spaces, Box, Space, Debug

export mkContainer, addBoxes, output

type Sleigh
    pos::(Int, Int)
    dim::Int
    boxes::Array{Box.Box3d, 1}
    sset::Spaces.SpaceSet
end

function mkSleigh()
    dim = 500
    sset = Spaces.mkSpaceSet(dim)
    Sleigh((0, 0), dim, Box.Box3d[], sset)
end

function addBoxes(slg::Sleigh, bs::Array{Box.Box3d, 1})
    slg.boxes = bs
    count = 0
    for box in bs
        space = Spaces.popBestAccomodation!(slg.sset, box)
        Space.orientTo!(space, box)
        intersects = Spaces.popIntersecting!(slg.sset, box)
        remainders = _calc_remainders(space, intersects, box)
        Spaces.push!(slg.sset, remainders)
        count = count + 1
        if true ###count % 10 == 0
            println("count: ", count)
            lenRem = length(remainders)
            println("remainders: $lenRem")
        end
    end
end

function _calc_remainders(best::Space.Space3d, intersects::Array{Space.Space3d, 1}, box::Box.Box3d)
    acc = Space.subtract(best, box)
    for s in intersects
        rs = Space.subtract(s, box)
        acc = vcat(acc, rs)
    end
    acc
end

function output(c::Sleigh)
    println("output to file...")
end

end
