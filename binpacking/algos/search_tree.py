
def mk_tree(bs):
    n = Node()
    for b in bs:
        n.add(b)
    return n

class Node(object):
    
    def __init__(self):
        self.surface_limit = 1000 * 1000
        self.height = 250
        self.children = [[]]
        self.heights = [0]

    def add(self, box):
        self._add([box.z, box.x, box.y], box)
        
    def _add(self, coords, box):
        head = coords[0]
        tail = coords[1:]
        if tail == []:
            if not self.children[head]:
                self.children[head] = Leaf()
            self.children[head]._add(box)
        else:
            if not self.children[head]:
                self.children[head] = Node()
            self.children[head]._add(tail, box)

    def delete(self, box):
        self._delete([box.z, box.x, box.y], box)
        self._delete([box.z, box.y, box.x], box)

    def _delete(self, coords, box):
        head = coords[0]
        tail = coords[1:]
        if tail == []:
            self.children[head]._delete(box)
        else:
            self.children[head]._delete(tail, box)        

    def find(self, z, xy_required, xy_asking):
        return self._find([z, xy_required, xy_asking])

    def _find(self, coords):
        head = coords[0]
        tail = coords[1:]
        if tail == []:
            leaf = self.children[head]
            return leaf.boxes if leaf else []
        else:
            node = self.children[head]
            if node:
                return node._find(tail)
            else:
                return []

    def print_z(self, z):
        if self.children[z]:
            self.children[z].print_all()

    def print_all(self):
        for c in self.children:
            if c: c.print_all()

class Leaf(object):
    
    def __init__(self):
        self.boxes = []

    def _add(self, box):
        self.boxes.append(box)

    def _delete(self, box):
        self.boxes.remove(box)
        
    def print_all(self):
        for b in self.boxes:
            print(str(b))
