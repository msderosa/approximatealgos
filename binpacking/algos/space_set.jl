require("data/rect.jl")
require("data/space.jl")
require("data/box.jl")
require("algos/space_list.jl")

module Spaces
using Space, Lists, Box, Rect

export mkSpaceSet, substract

type SpaceSet
    cursor::Int
    maxLimit::Int
    spacesAt::Dict{Int32,Lists.OrderedSpaceList}
end

function popIntersecting!(sset::SpaceSet, box::Box.Box3d)
    @assert Rect.zStart(box) == sset.cursor "operations should happen at the cursor level"
    acc = []
    for i in Rect.zStart(box):Rect.zEnd(box) - 1
        osl = get(sset.spacesAt, i, None)
        if osl != None
            intersecting = Lists.popIntersecting!(osl, box)
            acc = vcat(acc, intersecting)
        end
    end
    acc
end

function popBestAccomodation!(sset::SpaceSet, box::Box.Box3d)
    space = None
    while sset.cursor <= sset.maxLimit
        ol = get(sset.spacesAt, sset.cursor, None)
        if ol != None
            space = Lists.popBestAccomodation!(ol, box)
            if space == None
                sset.cursor = sset.cursor + 1
            else
                return space
            end
        else
            sset.cursor = sset.cursor + 1
        end
    end
    space
end

function push!(ss::SpaceSet, rs::Array{Space.Space3d, 1})
    print("cursor at: ", ss.cursor)
    for space in rs
        key = Rect.zStart(space)
        ol = get(ss.spacesAt, key, None)
        if ol == None
            newOl = Lists.mkOrderedSpaceList([space])
            setindex!(ss.spacesAt, newOl, key)
            if ss.maxLimit < key
                ss.maxLimit = key
            end
        else
            Lists.insert!(ol, space)
        end
    end
end
    
function mkSpaceSet(dim::Int)
    initialSpace = Space.Space3d((0,0,0), dim, dim)
    ordLs = Lists.mkOrderedSpaceList([initialSpace])
    SpaceSet(0, 0, [0 => ordLs])
end

end
