
import comp_geo.rectangles2d as R

class Node:
    
    def __init__(self, space, pos, is_end):
        self.space = space
        self.pos = pos
        self.is_end = is_end


def merge(spaces):
    merge_left_rightD(spaces)
    merge_top_bottomD(spaces)
    return spaces

def merge_left_rightD(spaces):
    ls = []
    for s in spaces:
        ls.append(Node(s, s.x_start(), 0))
        ls.append(Node(s, s.x_end(), 0.1))
    ls.sort(key=lambda nd: nd.pos + nd.is_end)

    compares = set()
    for node in ls:
        if node.is_end == 0:
            _expand_and_add_x(compares, node.space)
        else:
            compares.discard(node.space)
    
def merge_top_bottomD(spaces):
    ls = []
    for s in spaces:
        ls.append(Node(s, s.y_start(), 0))
        ls.append(Node(s, s.y_end(), 0.1))
    ls.sort(key=lambda nd: nd.pos + nd.is_end)

    compares = set()
    for node in ls:
        if node.is_end ==0:
            _expand_and_add_y(compares, node.space)
        else:
            compares.discard(node.space)

def _expand_and_add_y(compares, space):
    for comp in compares:
        if R.intersect(comp, space):
            if space.x_start() >= comp.x_start() and space.x_end() <= comp.x_end():
                if space.y_end() >= comp.y_start() and space.y_end() < comp.y_end():
                    space.y = space.y + (comp.y_end() - space.y_end())
                if space.y_start() > comp.y_start() and space.y_start() <= comp.y_end():
                    space.y = space.y + (space.y_start() - comp.y_start())
                    space.pos = (space.pos[0], comp.pos[1])
            if comp.x_start() >= space.x_start() and comp.x_end() <= space.x_end():
                if comp.y_end() >= space.y_start() and comp.y_end() < space.y_end():
                    comp.y = comp.y + (space.y_end() - comp.y_end())
                if comp.y_start() > space.y_start() and comp.y_start() <= space.y_end():
                    comp.y = comp.y + (comp.y_start() - space.y_start())
                    comp.pos = (comp.pos[0], space.pos[1])

    compares.add(space)

def _expand_and_add_x(compares, space):
    for comp in compares:
        if R.intersect(comp, space):
            if space.y_start() >= comp.y_start() and space.y_end() <= comp.y_end():
                if space.x_start() > comp.x_start() and space.x_start() <= comp.x_end():
                    space.x = space.x + (space.x_start() - comp.x_start())
                    space.pos = (comp.pos[0], space.pos[1])
                if space.x_end() >= comp.x_start() and space.x_end() < comp.x_end():
                    space.x = space.x + (comp.x_end() - space.x_end())
            if comp.y_start() >= space.y_start() and comp.y_end() <= space.y_end():
                if comp.x_start() > space.x_start() and comp.x_start() <= space.x_end():
                    comp.x = comp.x + (comp.x_start() - space.x_start())
                    comp.pos = (space.pos[0], comp.pos[1])
                if comp.x_end() >= space.x_start() and comp.x_end() < space.x_end():
                    comp.x = comp.x + (space.x_end() - comp.x_end())
    compares.add(space)

