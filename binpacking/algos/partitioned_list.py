
import comp_geo.hilbert_sort as H
import data.column as C
import math
import pdb

class PartitionedList:
    
    def __init__(self, bs, target_area):
        sorter = H.HilbertSort((2,250), (2,250))
        bs.sort(key=sorter.sort)
        self.box_count = len(bs)
        self.cols = self._partition(bs, target_area, 16, (None, None))
        self.sort_boxes_by_order()

    def _partition(self, bs, target_area, trial_count, tried):
        inc = len(bs) / trial_count
        if inc < 1:
            return [self._create_column(bs)]
        else:
            ls = []
            for i in list(range(trial_count)):
                ls.append(self._create_column(
                        self._part(bs, i, inc)))
            sm = sum([l.area_z() for l in ls])
            if sm < 1000 * 1000:
                count = self._get_larger_trial_count(trial_count, tried)
                if trial_count == count:
                    return ls
                else:
                    return self._partition(bs, target_area, count, (trial_count, tried[1]))
            else:
                count = self._get_smaller_trial_count(trial_count, tried)
                return self._partition(bs, target_area, count, (tried[0], trial_count))

    def _get_smaller_trial_count(self, current, tried):
        if tried[0] == None:
            return int(current / 2)
        elif tried[0] == current - 1:
            return current - 1
        else:
            half_way = (current - tried[0]) / 2
            return int(current - half_way)

    def _get_larger_trial_count(self, current, tried):
        if tried[1] == None:
            return current + current
        elif tried[1] == current + 1:
            return current
        else:
            half_way = (tried[1] - current) / 2
            return int(current + half_way)

    def area_z(self):
        return sum([c.area_z() for c in self.cols])

    def count(self):
        return len(self.cols)

    def sort_cols_by_max_dim(self):
        self.cols.sort(key=lambda col: max(col.x, col.y), reverse=True)

    def sort_cols_by_area(self):
        self.cols.sort(key=lambda col: col.x * col.y, reverse=True)

    def sort_boxes_by_order(self):
        for i, col in enumerate(self.cols):
            col.col_idx = i
            col.sort()

    def _create_column(self, bs):
        c = C.Column()
        for b in bs:
            c.add(b)
        return c

    def _part(self, bs, idx_low, inc):
        idx_high = idx_low + 1
        low = math.ceil(idx_low * inc)
        high = math.ceil(idx_high * inc)
        return bs[low:high]

    def _get_boxes_at(self, idx):
        ls = []
        for i, c in enumerate(self.cols):
            fst = snd = None
            box_count = len(c.boxes)
            if box_count > 1:
                fst = c.boxes[0]
                snd = c.boxes[1]
            elif box_count == 1:
                fst = c.boxes[0]
            elif box_count == 0:
                continue

            tpl = (fst, i, snd)
            ls.append(tpl)

        ls.sort(key=lambda tpl: tpl[0].order)
        return ls

    def balance(self, start_box_to_find, start_z_pos):
        balanced_cols = [C.Column(c.x, c.y) for c in self.cols]

        box_to_find = start_box_to_find
        z_pos = start_z_pos
        while box_to_find < self.box_count:
            print('next box: ', box_to_find, ', z_pos: ', z_pos)
            tpls = self._get_boxes_at(0)
            box_to_find, z_pos = self._distribute_horiz(box_to_find, z_pos, tpls, balanced_cols)
            self._remove_distributed(box_to_find)
        self.cols = balanced_cols

    def _distribute_horiz(self, box_to_find, z_pos, tpls, balanced_cols):
        alignment_positions = {}
        target = box_to_find
        for box, col_idx, top_nbr in tpls:
            col = balanced_cols[col_idx]
            if box.order == target and z_pos >= col.z:
                target = target + 1
                col.add_at_z(box, z_pos)
                if top_nbr:
                    alignment_positions[top_nbr.order] = col.z
            elif box.order == target and z_pos < col.z:
                alignment_positions[box.order] = col.z
                break
            else:
                break
        if target < self.box_count:
            new_z_pos = alignment_positions[target]
        else:
            new_z_pos = None
        return (target, new_z_pos)

    def _remove_distributed(self, box_to_find):
        for c in self.cols:
            if len(c.boxes) > 0:
                box = c.boxes[0]
                if box.order < box_to_find:
                    del c.boxes[0]
