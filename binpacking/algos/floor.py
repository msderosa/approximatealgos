
from numpy import *
import data.space2d as S
import itertools
import pdb

class Floor:

    def __init__(self, dim):
        self.spaces = set([S.Space2d((0,0), dim, dim)])
        self.free_space = dim * dim
        self.boxes_by_level = {}
        self.zlevel = 0

    def get_dimension(self):
        return self.grid.shape[0]

    def get_boxes_by_level(self, zlevel):
        boxes = []
        if zlevel in self.boxes_by_level:
            boxes = self.boxes_by_level[zlevel]
        return boxes

    def increment_zlevel(self, dim, recalc_spaces):
        if self.zlevel in self.boxes_by_level:
            del self.boxes_by_level[self.zlevel]
            
        self.zlevel = self.zlevel + 1
        if recalc_spaces:
            self.spaces = set([S.Space2d((0,0), dim, dim)])
            self.free_space = dim * dim
            self._recalc_spaces(self.zlevel)

    def _recalc_spaces(self, zlevel):
        boxes = self.get_boxes_by_level(zlevel)
        for box in boxes:
            self.free_space = self.free_space - box.area_z()
            
            #subtract out the box
            box_floor = box.floor()
            list_of_lists = map(lambda s: s.subtract(box_floor), self.spaces)
            remainders = itertools.chain(*list_of_lists)
            self.spaces = set(remainders)
        
    def place(self, box):
        space = self.get_acceptor(box)
        if space:
            self.place_at(box, space.pos)
            return space.pos
        else:
            return None

    def place_adapt_box(self, box):
        rotated_box, pos = self.get_acceptor_adapt_box(box)
        if rotated_box:
            self.place_at(rotated_box, pos)
            return (rotated_box, pos)
        else:
            return (None, None)

    def place_at(self, box, pos):
        x, y = pos
        box.pos = (x, y, self.zlevel)

        self._record_box_level_intersection(box)
        self._decrease_free_space(box.area_z())

        #subtract out the box
        box_floor = box.floor()
        list_of_lists = map(lambda s: s.subtract(box_floor), self.spaces)
        remainders = itertools.chain(*list_of_lists)
        self.spaces = set(remainders)

    def _record_box_level_intersection(self, box):
        for zlevel in range(self.zlevel + 1, box.z_end()):
            if zlevel not in self.boxes_by_level:
                self.boxes_by_level[zlevel] = []
            self.boxes_by_level[zlevel].append(box)

    def _decrease_free_space(self, area):
        self.free_space = self.free_space - area
        assert self.free_space >= 0 and self.free_space <= 1000 * 1000, "free area out of bounds"

    def get_acceptor(self, box):
        box_area = box.floor().area()
        spaces = filter(lambda s: s.area() >= box_area, sorted(self.spaces, key=lambda s: s.area()))

        accepts = None
        for space in spaces:
            if space.accomodates(box):
                accepts = space
                break
        return accepts

    def get_acceptor_adapt_box(self, box):
        min_box_area = box.dims[0] * box.dims[1]
        spaces = filter(lambda s: s.area() >= min_box_area, sorted(self.spaces, key=lambda s: s.area()))

        rotated_box = None
        pos = None
        for space in spaces:
            rotated_box, pos =  space.rotationally_accomodates_as(box)
            if rotated_box:
                break
        return (rotated_box, pos)
