require("data/space.jl")
require("data/box.jl")
require("data/rect.jl")
require("comp_geo/rectangles.jl")

module Lists
using Space, Box, Rect

export mkOrderedSpaceList, insert_all!, insert!, comparitor

type OrderedSpaceList
    spaces::Array{Space.Space3d, 1}
end

function popIntersecting!(osl::OrderedSpaceList, box::Box.Box3d)
    doIntsct = Space.Space3d[]
    dontIntsct = Space.Space3d[]
    for space in osl.spaces
        if Space.intersects(space, box)
            push!(doIntsct, space)
        else
            push!(dontIntsct, space)
        end
    end
    osl.spaces = dontIntsct
    doIntsct
end
    
function insert_all!(ss::OrderedSpaceList, rs::Array{Space.Space3d, 1})
    for remainder in rs
        insert!(ss, remainder)
    end
end

function popBestAccomodation!(ls::OrderedSpaceList, box::Box.Box3d)
    nSpaces = length(ls.spaces)
    println("searching for best of: ", nSpaces)
    if nSpaces == 0
        return None
    end
    idxStart = 1
    if nSpaces > 1
        idxStart = _position(ls, box, 1, endof(ls.spaces))
    end
    idxHit = None
    if idxStart != None
        state = idxStart
        while !done(ls.spaces, state)
            (space, state) = next(ls.spaces, state)
            if Space.accomodates(space, box)
                idxHit = state - 1
                break
            end
        end
    end
    if idxHit == None
        return None
    else
        return splice!(ls.spaces, idxHit)
    end
end

function insert!(ls, space::Space.Space3d)
    if length(ls.spaces) == 0
        Base.push!(ls.spaces, space)
        return
    end
    spaceVsMax = comparitor(space, ls.spaces[end])
    spaceVsMin = comparitor(space, ls.spaces[1])
    if spaceVsMax == 0 || spaceVsMin == 0
        return
    end
        
    if spaceVsMax == -1
        push!(ls.spaces, space)
    elseif spaceVsMin == 1
        unshift!(ls.spaces, space)
    else
        idx =  _position(ls, space, 1, length(ls.spaces))
        if idx != None
            Base.insert!(ls.spaces, idx + 1, space)
        end
    end
end

function _position(ls, rect::Rect.Rect3d, min::Int, max::Int)
    @assert min != max "unexpeted range equality"
    dx = max - min
    if dx == 1
        return min
    end
    mid = int(floor(dx / 2) + min)
    rectVsMid = comparitor(rect, ls.spaces[min])
    if rectVsMid == 0
        return None
    elseif rectVsMid == 1
        return _position(ls, rect, min, mid)
    else
        return _position(ls, rect, mid, max)
    end
end

function comparitor(a::Rect.Rect3d, b::Rect.Rect3d)
    da = Rect.zArea(b) - Rect.zArea(a)
    ret = sign(da)
    if ret == 0
        dx = b.x - a.x
        ret = sign(dx)
        if ret == 0
            dy = b.y - b.x
            ret = sign(dy)
        end
    end
    ret
end
    
function mkOrderedSpaceList(os::Array{Space.Space3d, 1})
    OrderedSpaceList(os)
end

end
