
import data.space2d as S
import comp_geo.rectangles2d as R
import policies.floor_placement as Pplace
import policies.space_sorting as Psort
import algos.floor as F
import pdb
import datetime

class Container:
    
    def __init__(self, dim):
        self.floor = F.Floor(dim)
        self.floor_area_total = dim * dim
        self.placed_boxes = {}
        self.pos = (0, 0)
        self.dim = dim

    def add_boxes(self, boxes):
        ok, rest_boxes = self._add_boxes_to_level(boxes, self.floor, 0)
        while ok and rest_boxes:
            self._increment_floor_zlevel()
            print('level: ', self.floor.zlevel)
            if self.floor.zlevel not in self.placed_boxes:
                continue
            ok, rest_boxes = self._add_boxes_to_level(rest_boxes, self.floor, 0)
        print("ok is: ", ok)

    def _increment_floor_zlevel(self):
        recalc_spaces = False
        next_zlevel = self.floor.zlevel + 1
        if next_zlevel in self.placed_boxes:
            recalc_spaces = True
        self.floor.increment_zlevel(self.dim, recalc_spaces)

    def _add_boxes_to_level(self, unplaced_boxes, floor, count):
        if len(unplaced_boxes) == 0:
            return (True, None)
        
        head = unplaced_boxes[0]
        pos = floor.place(head)
        if pos == None:
            rotated_box, pos = floor.place_adapt_box(head)
            if pos == None:
                print("ok: ", count, floor.free_space/self.floor_area_total)
                return (True, unplaced_boxes)
            else:
                self._store_box_in_container(rotated_box)
                return  self._add_boxes_to_level(unplaced_boxes[1:], floor, count + 1)
        else:
            self._store_box_in_container(head)
            return  self._add_boxes_to_level(unplaced_boxes[1:], floor, count + 1)

    def get_boxes_ending_at(self, zlevel):
        if zlevel in self.placed_boxes:
            return self.placed_boxes[zlevel]
        else:
            return []
    
    def _store_box_in_container(self, box):
        top = box.z_end()
        if top in self.placed_boxes:
            self.placed_boxes[top].add(box)
        else:
            self.placed_boxes[top] = set([box])

    def _place_boxD(self, space, box, level):
        pos, do_rotation = Pplace.position_btm_left(self, space, box.floor())
        if do_rotation:
            box.rotate90()
        box.pos = (pos[0], pos[1], level)

    def height_correction(self):
        max_height = max(list(self.placed_boxes.keys()))
        return max_height + 1

    def output(self):
        correction = self.height_correction()
        with open('SubmissionFile.csv', 'w') as hdl:
            self._output_header(hdl)
            for box_set in self.placed_boxes.values():
                for box in box_set:
                    recs = box.get_box_vertices()
                    self._output_box(hdl, recs, correction)
                
    def _output_header(self, hdl):
        hdl.write("PresentId,x1,y1,z1,x2,y2,z2,x3,y3,z3,x4,y4,z4,x5,y5,z5,x6,y6,z6,x7,y7,z7,x8,y8,z8\n")

    def _output_box(self, hdl, rec, correction):
        hdl.write("{}".format(rec[0]))
        for i in range(1,9):
            pnt = rec[i]
            hdl.write(",{},{},{}".format(pnt[0], pnt[1], correction - pnt[2]))
        hdl.write("\n")
    
