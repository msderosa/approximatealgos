
import unittest
import comp_geo.hilbert_sort as H
import data.box as B
import pdb

class TestHilbertCurve(unittest.TestCase):

      def test_sort1(self):
            hs = H.HilbertSort((5, 75), (5, 75))
            box = B.Box(1, 5, 5, 10)
            actual = hs.sort(box)
            self.assertEqual([0, 1], actual)

            
