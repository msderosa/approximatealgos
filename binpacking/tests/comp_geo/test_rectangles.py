
import unittest
import comp_geo.rectangles as Rs
import data.box as B
import data.rect as R
import pdb

class TestRectangles(unittest.TestCase):

      def test_intersection1(self):
            r1 = R.Rect((0,0,0), 10, 10, 10)
            r2 = R.Rect((4,4,-1), 2, 2, 2)
            r = Rs.intersection(r1, r2)
            self.assertEqual((4,4,0), r.pos)
            self.assertEqual([1,2,2], r.dims)

      def test_intersection_all(self):
            r1 = R.Rect((1,1,1), 2, 2, 2)
            r2 = R.Rect((0,0,0), 10, 10, 10)
            r = Rs.intersection(r1, r2)
            self.assertEqual((1,1,1), r.pos)
            self.assertEqual([2,2,2], r.dims)

      def test_intersection_bottom_corner(self):
            r1 = R.Rect((0,0,0), 5, 5, 5)
            r2 = R.Rect((0,0,0), 1, 4, 1)
            r = Rs.intersection(r1, r2)
            self.assertEqual([1, 1, 4], r.dims)
            self.assertEqual((0,0,0), r.pos)

      def test_orient_to(self):
            r = R.Rect((0,0,0), 2, 10, 4)
            b = B.Box(1, 3, 2, 5)
            actual = Rs.orient_box_to(r, b)
            self.assertEqual(1, actual.order)
            self.assertEqual(2, actual.x)
            self.assertEqual(5, actual.y)
            self.assertEqual(3, actual.z)

      def test_orient_to2(self):
            r = R.Rect((0,0,0), 5, 5, 5)
            b = B.Box(1, 3, 2, 5)
            actual = Rs.orient_box_to(r, b)
            self.assertEqual(1, actual.order)
            self.assertEqual(2, actual.x)
            self.assertEqual(3, actual.y)
            self.assertEqual(5, actual.z)
