
import unittest
import comp_geo.rectangles2d as Comp
import data.rect2d as R
import pdb

class TestRectangles(unittest.TestCase):

      def test_manhattan_dist(self):
            r1 = R.Rect2d((0,0), 5, 10)
            r2 = R.Rect2d(None, 4, 8)
            actual = Comp.manhattan_dist(r1, r2)
            self.assertEqual(3, actual)

      def test_manhattan_dist(self):
            r1 = R.Rect2d((0,0), 5, 10)
            r2 = R.Rect2d(None, 4, 8)
            actual = Comp.euclidian_dist(r1, r2)
            self.assertEqual(5, actual)

