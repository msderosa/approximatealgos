
import unittest
import comp_geo.hilbert_curve as H
from numpy import *
import pdb

class TestHilbertCurve(unittest.TestCase):

      def test_1x1curve(self):
            h = H.HilbertCurve(1)
            a = h.get_step(0, 0)
            self.assertEqual(0, a)

            a = h.get_step(1,0)
            self.assertIsNone(a)

      def test_rotate_p90(self):
            h = H.HilbertCurve(1)
            arr = array([1,2,3,4]).reshape(2,2)
            rot = h._rotate_p90(arr)
            self.assertEqual(4, rot[1,0])
            self.assertEqual(2, rot[1,1])
            self.assertEqual(3, rot[0,0])

      def test_rotate_n90(self):
            h = H.HilbertCurve(1)
            arr = array([1,2,3,4]).reshape(2,2)
            rot = h._rotate_n90(arr)
            self.assertEqual(2, rot[0,0])
            self.assertEqual(4, rot[0,1])
            self.assertEqual(1, rot[1,0])

      def test_reverse_numbering(self):
            h = H.HilbertCurve(1)
            arr = array([0,1,2,3]).reshape(2,2)
            rev = h._reverse_numbering(arr)
            self.assertEqual(3, rev[0,0])
            self.assertEqual(2, rev[0,1])

      def test_2x2curve(self):
            h = H.HilbertCurve(2)
            a = h.get_step(1,0)
            self.assertEqual(1, a)
            a = h.get_step(1,1)
            self.assertEqual(2, a)

      def test_3x3curve(self):
            h = H.HilbertCurve(4)
            a = h.get_step(0, 0)
            self.assertEqual(0, a)
            a = h.get_step(0, 3)
            self.assertEqual(15, a)
