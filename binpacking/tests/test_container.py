
import unittest
import container as C
import data.box as B
import pdb

class TestContainer(unittest.TestCase):

      def test_add_boxes_one_level(self):
            container = C.Container(10)
            boxes = [B.Box(1, 5, 5, 5),
                     B.Box(2, 5, 5, 5),
                     B.Box(3, 5, 5, 5),
                     B.Box(4, 5, 5, 5)]
            container.add_boxes(boxes)
            
            at_z5 = container.get_boxes_ending_at(5)
            self.assertEqual(4, len(at_z5))
            fst = at_z5.pop()
            self.assertTrue(fst.pos[0] <= 5)
            self.assertTrue(fst.pos[1] <= 5)
            self.assertEqual(0, fst.pos[2])

      def test_add_boxes_two_level(self):
            container = C.Container(10)
            boxes = [B.Box(1, 5, 5, 5),
                     B.Box(2, 5, 5, 5),
                     B.Box(3, 5, 5, 5),
                     B.Box(4, 5, 5, 5),
                     B.Box(5, 5, 5, 5)]
            container.add_boxes(boxes)

            self.assertEqual(5, container.floor.zlevel)
            at_z5 = container.get_boxes_ending_at(5)
            self.assertEqual(4, len(at_z5))
            
            at_z10 = container.get_boxes_ending_at(10)
            self.assertEqual(1, len(at_z10))
            fst = at_z10.pop()
            self.assertTrue(fst.pos[0] <= 5)
            self.assertTrue(fst.pos[1] <= 5)
            self.assertEqual(5, fst.pos[2])

      def test_newly_available_space(self):
            container = C.Container(10)
            boxes = [B.Box(1, 5, 5, 5),
                     B.Box(2, 5, 5, 5),
                     B.Box(3, 5, 5, 5),
                     B.Box(4, 5, 5, 5)]
            container.add_boxes(boxes)

            bs = container.get_boxes_ending_at(1)
            self.assertEqual([], bs)
            bs = container.get_boxes_ending_at(5)
            self.assertEqual(4, len(bs))
