
import unittest
import container as Cnt
import data.space2d as S
import data.rect2d as R
import policies.floor_placement as Pol
import pdb

class TestFloorPlacement(unittest.TestCase):

      def test_favoring_corners1(self):
            cont = Cnt.Container(10) 
            space = S.Space2d((6,6), 4, 4)
            floor = R.Rect2d(None, 2, 3)
            pos, is_rotated = Pol.position_favoring_edges(cont, space, floor)
            self.assertEqual((8,7), pos)
            self.assertFalse(is_rotated)

      def test_favoring_corners2(self):
            cont = Cnt.Container(10) 
            space = S.Space2d((8,0), 2, 10)
            floor = R.Rect2d(None, 4, 2)
            pos, is_rotated = Pol.position_favoring_edges(cont, space, floor)
            self.assertEqual((8,0), pos)
            self.assertTrue(is_rotated)

      def test_favoring_corners3(self):
            cont = Cnt.Container(1000) 
            space = S.Space2d((0,250), 1000, 557)
            floor = R.Rect2d(None, 7, 4)
            pos, is_rotated = Pol.position_favoring_edges(cont, space, floor)
            self.assertEqual((0,250), pos)
            self.assertTrue(is_rotated)


                       
