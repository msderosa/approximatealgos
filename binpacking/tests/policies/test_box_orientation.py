
import unittest
import policies.box_orientation as P
import data.box as B
import pdb

class TestBoxOrientation(unittest.TestCase):

      def test_rotate_random(self):
            b = B.Box(1, 3, 4, 2)
            v1 = b.volume()
            P.randomD(b)
            v2 = b.volume()
            self.assertEqual(v1, v2)


                       
