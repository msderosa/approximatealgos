
import unittest
import data.box as B

class TestBox(unittest.TestCase):

      def test_not_equal_compare_by_uid(self):
            b1 = B.Box(1, 2, 3, 4)
            b2 = B.Box(2, 2, 3, 4)
            self.assertNotEqual(b1, b2)
            self.assertNotEqual(hash(b1), hash(b2))

      def test_equal_compare_by_uid(self):
            b1 = B.Box(1, 2, 3, 4)
            b2 = B.Box(1, 2, 3, 4)
            self.assertEqual(b1, b2)      
            self.assertEqual(hash(b1), hash(b2))
            
      def test_get_box_vertices_no_offset(self):
            b = B.Box(1, 2, 3, 4)
            b.pos = (2,2,2)
            exp = [1, (3,3,3), (4,3,3), (4,5,3), (3,5,3), 
                   (3,3,6), (4,3,6), (4,5,6), (3,5,6)]
            actual = b.get_box_vertices()
            self.assertEqual(exp, actual)

