
import unittest
import data.space as S
import data.box as B

class TestSpace(unittest.TestCase):

      def test_accomodate_true(self):
            s = S.Space((0, 0, 0), 25, 25, 25)
            b = B.Box(1, 2, 2, 3)
            actual = s.accomodates(b)
            self.assertTrue(actual[0])
            self.assertTrue(actual[1] > 0)

      def test_accomodate_false(self):
            s = S.Space((0, 0, 0), 25, 25, 25)
            b = B.Box(1, 1, 1, 26)
            actual = s.accomodates(b)
            self.assertFalse(actual[0])

      def test_subtract(self):
            s = S.Space((0, 0, 0), 25, 25, 25)
            b = B.Box(1, 2, 2, 2)
            b.pos = (0,0,0)
            actual = s.subtract(b)
            self.assertEqual(3, len(actual))
            fst = actual[0]
            self.assertEqual((2, 0, 0), fst.pos)
            self.assertEqual([23, 25, 25], fst.dims)

            snd = actual[1]
            self.assertEqual((0, 2, 0), snd.pos)
            self.assertEqual([23, 25, 25], snd.dims)

            thd = actual[2]
            self.assertEqual((0, 0, 2), thd.pos)
            self.assertEqual([23, 25, 25], thd.dims)            
            
      def test_subtract_all(self):
            s = S.Space((0, 0, 0), 25, 25, 25)
            b = B.Box(1, 25, 25, 25)
            b.pos = (0,0,0)
            actual = s.subtract(b)
            self.assertEqual(0, len(actual))

      def test_subtact_for_two_divisions(self):
            s = S.Space((0, 0, 0), 25, 25, 25)
            b = B.Box(1, 25, 5, 5)
            b.pos = (0,0,0)
            actual = s.subtract(b)
            self.assertEqual(2, len(actual))            
            fst = actual[0]
            self.assertEqual([20, 25, 25], fst.dims)
            snd = actual[1]
            self.assertEqual([20, 25, 25], snd.dims)

      
      def test_subtract_non_intersecting(self):
            s = S.Space((0, 0, 0), 10, 10, 10)
            b = B.Box(1, 5, 5, 5)
            b.pos = (12, 12, 12)
            actual = s.subtract(b)
            self.assertEqual(1, len(actual))
            self.assertIs(s, actual[0])

      def test_a_pole_halfway_through_center(self):
            s = S.Space((0,0,0), 10, 10, 10)
            b = B.Box(1, 2, 2, 7)
            b.pos = (4, 4, -2)
            actual = s.subtract(b)
            self.assertEqual(5, len(actual))
            fst = actual[0]
            self.assertEqual([4, 10, 10], fst.dims)
            lst = actual[-1]
            self.assertEqual([5,10,10], lst.dims)

