require("data/space.jl")
require("data/box.jl")

using Base.Test, Space

# test orientTo set position
@test begin
    space = Space.Space3d((2,3,4), 4, 7)
    box = Box.Box3d(1, None, 2, 2, 2)
    Space.orientTo!(space, box)
    @test (2,3,4) == box.pos

    box = Box.Box3d(1, None, 5, 2, 2)
    Space.orientTo!(space, box)
    @test box.x == 2
    @test box.y == 5
    true
end

# test subtract
@test begin
    space = Space.Space3d((1,1,1), 10, 10)
    box = Box.Box3d(5, (4,0,0), 1, 2, 2)
    rs = Space.subtract(space, box)
    @test 4 == length(rs)

    fst = first(rs)
    @test fst.x == 3
    @test fst.y == 10
    lst = last(rs)
    @test lst.x == 10
    @test lst.y == 10
    @test lst.pos == (1,1,2)
    true
end

# test intersects 
@test begin
    space = Space.Space3d((1,1,1), 10, 10)
    box = Box.Box3d(5, (4,0,0), 1, 2, 2)
    Space.intersects(space, box)
end

# test no intersection
@test begin
    space = Space.Space3d((1,1,1), 10, 10)
    box = Box.Box3d(5, (14,0,0), 1, 2, 2)
    false == Space.intersects(space, box)
end






