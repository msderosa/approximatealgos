
import unittest
import data.space2d as S
import data.rect2d as R
import data.box as B
import pdb

class TestSpace2d(unittest.TestCase):

      def test_accomodate_true(self):
            s = S.Space2d((0, 0), 4, 12)
            r = R.Rect2d((0, 0), 3, 8)
            actual = s.accomodates(r)
            self.assertTrue(actual)

      def test_accomodate_false(self):
            s = S.Space2d((0, 0), 25, 25)
            b = R.Rect2d((0, 0), 1, 26)
            actual = s.accomodates(b)
            self.assertFalse(actual)

      def test_subtract(self):
            s = S.Space2d((0, 0), 25, 25)
            b = R.Rect2d((0, 0), 2, 2)
            actual = s.subtract(b)
            self.assertEqual(2, len(actual))
            fst = actual[0]
            self.assertEqual((2, 0), fst.pos)
            self.assertEqual([23, 25], fst.get_dims())

            snd = actual[1]
            self.assertEqual((0, 2), snd.pos)
            self.assertEqual([23, 25], snd.get_dims())

      def test_subtract_all(self):
            s = S.Space2d((0, 0), 25, 25)
            b = R.Rect2d((0, 0), 25, 25)
            actual = s.subtract(b)
            self.assertEqual(0, len(actual))

      def test_subtact_for_two_divisions(self):
            s = S.Space2d((0, 0), 25, 25)
            b = R.Rect2d((0, 0), 25, 5)
            actual = s.subtract(b)
            self.assertEqual(1, len(actual))            
            fst = actual[0]
            self.assertEqual([20, 25], fst.get_dims())
      
      def test_subtract_non_intersecting(self):
            s = S.Space2d((0, 0), 10, 10)
            b = R.Rect2d((12, 12), 5, 5)
            actual = s.subtract(b)
            self.assertEqual(1, len(actual))
            self.assertIs(s, actual[0])

      def test_a_pole_halfway_through_center(self):
            s = S.Space2d((0,0), 10, 10)
            b = R.Rect2d((4, -1), 2, 2)
            actual = s.subtract(b)
            self.assertEqual(3, len(actual))
            fst = actual[0]
            self.assertEqual([4, 10], fst.get_dims())
            lst = actual[-1]
            self.assertEqual([9, 10], lst.get_dims())

      def test_subtract_a_center_intrusion(self):
            s = S.Space2d((0,0), 10, 10)
            b = R.Rect2d((4, 4), 2, 2)
            actual = s.subtract(b)
            self.assertEqual(4, len(actual))
            for a in actual:
                  self.assertEqual(40, a.area())

      def test_subtract_bug1(self):
            s = S.Space2d((878, 189), 122, 811)
            r = R.Rect2d((693, 225), 221, 161)
            actual = s.subtract(r)
            self.assertEqual([86, 811], actual[0].get_dims())
            self.assertEqual([36, 122], actual[1].get_dims())
            self.assertEqual([122, 614], actual[2].get_dims())

      def test_rot_accomodates_as(self):
            s = S.Space2d((0, 0), 2, 10)
            box = B.Box(1, 4, 5, 2)
            
            actual = s.accomodates(box)
            self.assertFalse(actual)
            
            rotated_box, pos = s.rotationally_accomodates_as(box)
            self.assertEqual(2, rotated_box.x)
            self.assertEqual(4, rotated_box.y)
            self.assertEqual(5, rotated_box.z)

            
