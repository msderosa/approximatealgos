
import unittest
import data.box as Bx
import data.bin as Bn
import pdb

class TestBin(unittest.TestCase):

      def test_affinity(self):
            bin = Bn.Bin((0,0,0), 25, 25, 25)
            box = Bx.Box(1, 3, 3, 3)
            self.assertEqual(0, bin.affinity(box))

      def test_affinity_not_accepted(self):
            bin = Bn.Bin((0,0,0), 25, 25, 25)
            box = Bx.Box(1, 3, 3, 33)
            self.assertEqual(-1, bin.affinity(box))

      def test_simple_add(self):
            bin = Bn.Bin((0,0,0), 10, 10, 10)
            box1 = Bx.Box(1, 5, 5, 1)
            bin.add(box1)
            self.assertEqual(1, len(bin.boxes))
            self.assertIsNotNone(bin.boxes[0].pos)
            
            box2 = Bx.Box(2, 9, 9, 9)
            aff = bin.affinity(box2)
            self.assertEqual(0, aff)
            bin.add(box2)
            self.assertEqual(2, len(bin.boxes))
            
            

