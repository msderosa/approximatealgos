
import unittest
import data.rect2d as R
import pdb

class TestRect2d(unittest.TestCase):

      def test_equal(self):
            r1 = R.Rect2d((5,5), 2, 11)
            r2 = R.Rect2d((5,5), 2, 11)
            r3 = R.Rect2d((5,5), 2, 10)
            self.assertEqual(r1, r2)
            self.assertNotEqual(r1, r3)
