
import unittest
import data.box as B
import ops.stats as S
import tests.factories.box_factory as Fct

class TestStats(unittest.TestCase):

    def test_max_dimensions(self):
        bs = Fct.five_unit_boxes()
        a = S.max_dimensions(bs)
        self.assertEqual(
            (1, 1, 1), a)

    def test_extrace_layer_face_sample(self):
        bs = Fct.five_unit_boxes()
        bs.append(B.Box(6, 2, 4, 3))
        ls, stats = S.extract_layer_face_sample(bs)
        self.assertEqual(6, len(ls))            
        self.assertEqual(13, stats['face_area'])
        self.assertEqual(3, stats['z_max'])
        self.assertEqual(5, stats['z_max_pos'])
