
import unittest
import ops.lists as L
import data.box as B
import tests.factories.box_factory as Fct
import pdb

class TestLists(unittest.TestCase):

      def test_take1(self):
            bs = Fct.five_unit_boxes()
            a, face_area = L.take(bs, 3)
            self.assertEqual(3, len(a))
            self.assertEqual(3, face_area)
      
      def test_take_nothing(self):
            bs = Fct.five_unit_boxes()
            a, face_area = L.take(bs, 0.5)
            self.assertEqual(0, len(a))
            self.assertEqual(0, face_area)

      def test_take_more_than_exists(self):
            bs = Fct.five_unit_boxes()
            a, face_area = L.take(bs, 10)
            self.assertEqual(5, len(a))            
            self.assertEqual(5, face_area)

      def test_take_partial(self):
            bs = Fct.five_seq_increasing()
            a, face_area = L.take(bs, 16)
            self.assertEqual(3, len(a))            
            self.assertEqual(14, face_area)

      def test_hibert_sort(self):
            bs = Fct.three_random()
            bs.insert(0, B.Box(4, 2, 2, 5))
            ls = L.hilbert_sort(1, 8, bs)
            self.assertEqual([1,2,2], ls[0].dims)
            self.assertEqual([2,2,5], ls[1].dims)
            self.assertEqual([1,6,8], ls[2].dims)
