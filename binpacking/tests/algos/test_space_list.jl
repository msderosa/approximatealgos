
require("algos/space_list.jl")
require("data/space.jl")
require("data/box.jl")

using Base.Test, Lists, Space

# test creation
@test begin
    temp = [Space.Space3d((1,1,1), 2, 2)]
    ls = Lists.mkOrderedSpaceList(temp)
    @test ls != None
    @test length(ls.spaces) == 1
    true
end

# test comparitor
@test begin
    smaller = Space.Space3d((1,1,1), 2, 2)
    bigger = Space.Space3d((1, 12, 2), 2, 3)
    actual = Lists.comparitor(smaller, bigger)
    @test actual == 1

    actual = Lists.comparitor(bigger, smaller)
    @test actual == -1
    true
end

# test comparitor equality
@test begin
    smaller = Space.Space3d((1, 1, 1), 2, 2)
    bigger = Space.Space3d((1, 1, 1), 2, 2)
    actual = Lists.comparitor(smaller, bigger)
    actual == 0
end

# test insertion
@test begin
    temp = [Space.Space3d((1,1,1), 2, 2)]
    ls = Lists.mkOrderedSpaceList(temp)
    @test length(ls.spaces) == 1

    a = Space.Space3d((2,2,2), 2, 3)
    Lists.insert!(ls, a)
    @test length(ls.spaces) == 2

    b = Space.Space3d((3,3,3), 50, 50)
    Lists.insert!(ls, b)
    c = Space.Space3d((4,4,4), 4, 4)
    Lists.insert!(ls, c)
    @test length(ls.spaces) == 4

    third = ls.spaces[3]
    @test third.x == 4 && third.y == 4 
    true
end

#test duplicate insertion
@test begin
    temp = [Space.Space3d((1,1,1), 2, 2)]
    ls = Lists.mkOrderedSpaceList(temp)

    a = Space.Space3d((4,4,4), 5, 5)
    b = Space.Space3d((3,3,3), 4, 4)
    c = Space.Space3d((2,2,2), 3, 3)
    d = Space.Space3d((3,3,3), 4, 4)
    Lists.insert_all!(ls, [a,b,c,d])
    length(ls.spaces) == 4
end

# test best accomodation, none exists
@test begin
    temp = [Space.Space3d((1,1,1), 2, 2)]
    ls = Lists.mkOrderedSpaceList(temp)

    b = Box.Box3d(1, None, 3, 4, 7)
    space = Lists.popBestAccomodation!(ls, b)
    space == None
end

# test best accomodation, one exists
@test begin
    temp = [Space.Space3d((1,1,1), 2, 2)]
    ls = Lists.mkOrderedSpaceList(temp)

    a = Space.Space3d((4,4,4), 5, 5)
    b = Space.Space3d((2,2,2), 3, 3)
    c = Space.Space3d((3,3,3), 4, 4)
    Lists.insert_all!(ls, [a,b,c])

    box = Box.Box3d(2, None, 4, 5, 7)
    space = Lists.popBestAccomodation!(ls, box)
    space.x == 5 && space.y == 5
end

# test pop best accomodation first query
@test begin
    initSpace = Space.Space3d((0,0,0), 1000, 1000)
    osl = Lists.mkOrderedSpaceList([initSpace])

    aBox = Box.Box3d(1, None, 3, 3, 5)
    best = Lists.popBestAccomodation!(osl, aBox)
    initSpace == best
end
