
import unittest
import algos.floor as F
import data.box as B
import pdb

class TestHilbertCurve(unittest.TestCase):

      def test_init(self):
            floor = F.Floor(10)
            self.assertEqual(100, floor.free_space)

      def test_accepts(self):
            floor = F.Floor(10)
            box = B.Box(1, 4, 4, 4)
            actual = floor.get_acceptor(box)
            self.assertIsNotNone(actual)

            box = B.Box(1, 11, 4, 4)
            actual = floor.get_acceptor(box)
            self.assertIsNone(actual)

      def test_place_at(self):
            floor = F.Floor(10)
            box = B.Box(1, 4, 4, 4)
            floor.place_at(box, (0,0))
            self.assertEqual(84, floor.free_space)
            self.assertEqual((0,0,0), box.pos)
            self.assertEqual(2, len(floor.spaces))

      def test_place_at2(self):
            floor = F.Floor(10)
            box1 = B.Box(1, 2, 2, 4)
            box2 = B.Box(2, 2, 5, 5)
            floor.place_at(box1, (0,0))
            floor.place_at(box2, (2,0))
            self.assertEqual(86, floor.free_space)
            self.assertEqual(5, len(floor.spaces))

      def test_place(self):
            floor = F.Floor(10)
            box1 = B.Box(1, 8, 2, 4)
            box2 = B.Box(2, 4, 7, 5)
            box3 = B.Box(3, 7, 2, 6)
            actual = floor.place(box1)
            self.assertEqual((0,0), actual)

            actual = floor.place(box2)
            self.assertEqual((0,2), actual)
            
            actual = floor.place(box3)
            self.assertIsNone(actual)

      def test_place2(self):
            floor = F.Floor(10)
            box1 = B.Box(1, 8, 2, 4)
            box2 = B.Box(2, 4, 7, 5)
            box3 = B.Box(3, 7, 2, 6)
            actual = floor.place(box1)
            self.assertEqual((0,0), actual)

            actual = floor.place(box2)
            self.assertEqual((0,2), actual)
            
            actual = floor.place(box3)
            self.assertIsNone(actual)            
            
      def test_free_area1(self):
            floor = F.Floor(10)
            box1 = B.Box(1, 5, 5, 4)
            actual = floor.place(box1)
            self.assertEqual(75, floor.free_space)

            floor.increment_zlevel(10, False)
            self.assertEqual(1, floor.zlevel)
            self.assertEqual(75, floor.free_space)            

      def test_increment_zlevel(self):
            floor = F.Floor(10)
            box1 = B.Box(1, 5, 5, 4)
            box2 = B.Box(2, 2, 5, 1)
            floor.place(box1)
            floor.place(box2)
            self.assertEqual(65, floor.free_space)
            self.assertEqual(3, len(floor.spaces))

            floor.increment_zlevel(10, True)
            self.assertEqual(75, floor.free_space)
            self.assertEqual(2, len(floor.spaces))
