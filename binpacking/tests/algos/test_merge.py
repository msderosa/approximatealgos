
import unittest
import algos.merge as M
import data.space2d as S
import pdb

class TestPartitionedList(unittest.TestCase):
    
    def test_merge_in_xdirection1(self):
        s1 = S.Space2d((4,4), 10, 10)
        s2 = S.Space2d((14,6), 5, 5)
        actuals = M.merge([s1, s2])
        self.assertEqual(2, len(actuals))
        
        a = actuals[0]
        self.assertEqual((4,4), a.pos)
        self.assertEqual(10, a.x)
        self.assertEqual(10, a.y)
        a = actuals[1]
        self.assertEqual((4,6), a.pos)
        self.assertEqual(15, a.x)
        self.assertEqual(5, a.y)

    def test_merge_in_xdirection2(self):
        s1 = S.Space2d((4,4), 10, 10)
        s2 = S.Space2d((6,6), 5, 5)
        actuals = M.merge([s1, s2])
        self.assertEqual(2, len(actuals))
        
        a = actuals[0]
        self.assertEqual((4,4), a.pos)
        self.assertEqual(10, a.x)
        self.assertEqual(10, a.y)
        a = actuals[1]
        self.assertEqual((4,4), a.pos)
        self.assertEqual(10, a.x)
        self.assertEqual(10, a.y)

    def test_merge_in_ydirection(self):
        s1 = S.Space2d((0,5), 10, 10)
        s2 = S.Space2d((5,0), 5, 5)
        actuals = M.merge([s1, s2])
        self.assertEqual(2, len(actuals))
        
        a = actuals[0]
        self.assertEqual((0,5), a.pos)
        self.assertEqual(10, a.x)
        self.assertEqual(10, a.y)
        a = actuals[1]
        self.assertEqual((5,0), a.pos)
        self.assertEqual(5, a.x)
        self.assertEqual(15, a.y)



