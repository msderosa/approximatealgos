
import unittest
import algos.partitioned_list as Pl
import data.column as C
import data.box as B
import tests.factories.box_factory as BFct
import pdb

class TestPartitionedList(unittest.TestCase):
    
    def test_initialization(self):
        bs = BFct.five_seq_increasing2()
        ls = Pl.PartitionedList(bs, 1000)
        ls.balance(1, 0)
        self.assertEqual(1, len(ls.cols))

    def test_balance(self):
        ls = Pl.PartitionedList([], 1000)
        c1 = C.Column()
        c1.add(B.Box(1, 1, 1, 2))
        c1.add(B.Box(7, 1, 1, 2))
        c2 = C.Column()
        c2.add(B.Box(2, 1, 1, 1))
        c2.add(B.Box(5, 1, 1, 2))
        c3 = C.Column()
        c3.add(B.Box(3, 1, 1, 2))
        c3.add(B.Box(4, 1, 1, 1))
        c3.add(B.Box(6, 1, 1, 1))
        ls.cols = [c1, c2, c3]
        ls.box_count = 7
        ls.balance(1, 0)
        for c in ls.cols:
            self.assertEqual(3, len(c.boxes))
        col = ls.cols[0]
        self.assertEqual(0, col.boxes[0].pos[2])
        self.assertEqual(2, col.boxes[1].pos[2])
        self.assertEqual(3, col.boxes[2].pos[2])

        col = ls.cols[1]
        self.assertEqual(0, col.boxes[0].pos[2])
        self.assertEqual(1, col.boxes[1].pos[2])
        self.assertEqual(2, col.boxes[2].pos[2])

        col = ls.cols[2]
        self.assertEqual(0, col.boxes[0].pos[2])
        self.assertEqual(2, col.boxes[1].pos[2])
        self.assertEqual(3, col.boxes[2].pos[2])



