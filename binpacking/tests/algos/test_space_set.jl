require("algos/space_set.jl")
require("data/box.jl")

using Base.Test, Spaces, Box

# test nothing
@test begin
    1 == 1
end

# test creation
@test begin
    spaces = Spaces.mkSpaceSet(1000)
    spaces != None
end

# test push
@test begin
    spaces = Spaces.mkSpaceSet(1000)
    @test spaces.maxLimit == 0
    s1 = Space.Space3d((4, 4, 2), 2, 2)
    s2 = Space.Space3d((7, 2, 3), 2, 2)
    Spaces.push!(spaces, [s1, s2])
    spaces.maxLimit == 3
end

# test find best accomodation
@test begin
    sset = Spaces.mkSpaceSet(1000)
    s1 = Space.Space3d((4, 4, 2), 2, 2)
    s2 = Space.Space3d((7, 2, 3), 4, 7)
    Spaces.push!(sset, [s1, s2])
    sset.cursor = 1

    box = Box.Box3d(1, None, 3, 5, 12)
    actual = Spaces.popBestAccomodation!(sset, box)
    s2 == actual
end

# test pop intersecting
@test begin
    sset = Spaces.mkSpaceSet(1000)
    s1 = Space.Space3d((0, 0, 0), 5, 5)
    s2 = Space.Space3d((10, 10, 10), 5, 5)
    s3 = Space.Space3d((15, 15, 15), 5, 5)
    Spaces.push!(sset, [s1, s2, s3])

    box = Box.Box3d(1, (0,0,0), 12, 12, 12)
    actual = Spaces.popIntersecting!(sset, box)
    2 == length(actual)
end

# test initial box query for spaces
@test begin
    sset = Spaces.mkSpaceSet(1000)
    box = Box.Box3d(1, None, 2, 7, 2)
    space = Spaces.popBestAccomodation!(sset, box)
    @test space != None
    @test space.x == 1000
    @test space.y == 1000
    true
end
