
import data.box as B

def five_unit_boxes():
    return [B.Box(1, 1, 1, 1), B.Box(2, 1, 1, 1), B.Box(3, 1, 1, 1), B.Box(4, 1, 1, 1), B.Box(5, 1, 1, 1)]

def five_seq_increasing():
        return [B.Box(1, 1, 1, 1), B.Box(2, 2, 2, 1), B.Box(3, 3, 3, 1), B.Box(4, 4, 4, 1), B.Box(5, 5, 5, 1)]

def five_seq_increasing2():
        return [B.Box(1, 2, 2, 2), B.Box(2, 3, 3, 3), B.Box(3, 4, 4, 4), B.Box(4, 5, 5, 5), B.Box(5, 6, 6, 6)]

def three_random():
    return [B.Box(1, 5, 6, 3), B.Box(2, 2, 2, 1), B.Box(3, 6, 1, 8)]
