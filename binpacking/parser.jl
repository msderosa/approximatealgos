require("data/box.jl")
require("policies/policy.jl")

module Parser
using Box
using Policy

export parse

function parse()
    bs = Box.Box3d[]
    ls = open(readlines, "presents.csv")
    for line in ls
        temp = chomp(line)
        if isdigit(temp[1])
            els = split(temp, ',')
            box = Box.mkBox(int(els[1]), 
                            int(els[2]), int(els[3]), int(els[4]),
                            Policy.Orientation.xLargeZSmall!)
            push!(bs, box)
        end
    end
    bs
end

end
## notes
## 2 - 10
## 5 - 70
## 65 - 250
