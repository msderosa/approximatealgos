
import config as Cfg
import algos.search_tree as St
import pdb

def pack(bs):
    search_tree = St.mk_tree(bs)
    cs = _pack_x(bs, search_tree)
    print("x reduction: {} -> {}".format(len(bs), len(cs)))

    search_tree = St.mk_tree(cs)
    ds = _pack_y(cs, search_tree)
    print("y reduction: {} -> {}".format(len(cs), len(ds)))
    if len(ds) == len(bs):
        return ds
    else:
        return pack(ds)

def _pack_x(bs, st):
    ls = []
    merged = set()
    for b in bs:
        if b in merged: continue

        if b.x in Cfg.target_xy_multipliers:
            ls.append(b)
        else:
            target_xs = _target_lengths(b.x)
            packable_bs = _find_smallest_x_companions(b, st, target_xs)
            if len(packable_bs) == 0:
                ls.append(b)
            else:
                packable_box = packable_bs[0]
                ls.append(b.pack_x(packable_box))
                merged.add(packable_box)
                st.delete(packable_box)
        st.delete(b)
    return ls

def _pack_y(bs, st):
    ls = []
    merged = set()
    for b in bs:
        if b in merged: continue

        if b.y in Cfg.target_xy_multipliers:
            ls.append(b)
        else:
            target_ys = _target_lengths(b.y)
            packable_bs = _find_smallest_y_companions(b, st, target_ys)
            if len(packable_bs) == 0:
                ls.append(b)
            else:
                packable_box = packable_bs[0]
                ls.append(b.pack_y(packable_box))
                merged.add(packable_box)
                st.delete(packable_box)
        st.delete(b)
    return ls

def _find_smallest_x_companions(box, st, target_xs):
    if target_xs == []:
        return []
    else:
        target_x = target_xs[0]
        companions = [c for c in st.find(box.z, box.y, target_x - box.x) if c != box]
        if companions == []:
            return _find_smallest_x_companions(box, st, target_xs[1:])
        else:
            return companions

def _find_smallest_y_companions(box, st, target_ys):
    if target_ys == []:
        return []
    else:
        target_y = target_ys[0]
        companions = [c for c in st.find(box.z, box.x, target_y - box.y) if c != box]
        if companions == []:
            return _find_smallest_y_companions(box, st, target_ys[1:])
        else:
            return companions
    
def _target_lengths(dist):
    return [tgt for tgt in Cfg.target_xy_multipliers if tgt > dist]
