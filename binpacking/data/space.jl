require("data/rect.jl")
require("data/box.jl")
require("comp_geo/points.jl")

module Space
using Rect, Points, Box

export Space3d, mkVerticallyInf, accomodates, subtract

type Space3d <: Rect.Rect3d
    pos::(Int, Int, Int)
    x::Int
    y::Int
end    

function mkVerticallyInf()
    Space3d((0, 0, 0), 0, 0)
end

# retuns an indication of the ability of the space to hold
# a box assuming not z direction change
function accomodates(s::Space3d, r::Rect.Rect3d)
    return (r.x <= s.x && r.y <= s.y) || (r.y <= s.x && r.x <= s.y)
end

function orientTo!(s::Space3d, r::Rect.Rect3d)
    xy = (r.x, r.y)
    if xy[1] > s.x || xy[2] > s.y
        r.x = xy[2]
        r.y = xy[1]
    end
    r.pos = s.pos
    @assert r.x <= s.x "x outside bounds, call accepts first"
    @assert r.y <= s.y "y outside bounds, call accepts first"
end

# subtract :: Space -> Box -> [Space]
function subtract(s::Space3d, b::Rect.Rect3d)
    @assert Rect.zStart(b) <= Rect.zStart(s) "always accumulate from below"
    @assert Rect.zEnd(b) > Rect.zStart(s) "only subtract where needed"
    r = Space.intersection(s, b)
    temp = Space3d[]
    if r == None
        return temp.push!(s)
    end

    if Rect.xStart(r) > Rect.xStart(s)
        xDim = Rect.xStart(r) - Rect.xStart(s)
        push!(temp, Space3d(s.pos, xDim, s.y))
    end
    if Rect.xEnd(r) < Rect.xEnd(s)
        xDim = Rect.xEnd(s) - Rect.xEnd(r)
        dx = Rect.xEnd(r) - Rect.xStart(s)
        org = Points.translate(s.pos, dx, 0, 0)
        push!(temp, Space3d(org, xDim, s.y))
    end
    if Rect.yStart(r) > Rect.yStart(s)
        yDim = Rect.yStart(r) - Rect.yStart(s)
        push!(temp, Space3d(s.pos, s.x, yDim))
    end
    if Rect.yEnd(r) < Rect.yEnd(s)
        yDim = Rect.yEnd(s) - Rect.yEnd(r)
        dy = Rect.yEnd(r) - Rect.yStart(s)
        org = Points.translate(s.pos, 0, dy, 0)
        push!(temp, Space3d(org, s.x, yDim))
    end
    dz = Rect.zEnd(r) - Rect.zStart(s)
    org = Points.translate(s.pos, 0, 0, dz)
    push!(temp, Space3d(org, s.x, s.y))
    temp
end

function intersects(s::Space3d, r::Rect.Rect3d)
    doesit = false
    rxs = Rect.xStart(r)
    rxe = Rect.xEnd(r)
    sxs = Rect.xStart(s)
    sxe = Rect.xEnd(s)
    xintersects = (sxs < rxs && rxs < sxe) || (sxs < rxe && rxe < sxe) || (rxs < sxs && sxe < rxe)
    if xintersects
        rys = Rect.yStart(r)
        rye = Rect.yEnd(r)
        sys = Rect.yStart(s)
        xye = Rect.yEnd(s)
        yintersects = (sys < rys && rys < xye) || (sys < rye && rye < xye) || (rys < sys && xye < rye)
        if yintersects
            doesit = (Rect.zEnd(r) > Rect.zStart(s))
        end
    end
    doesit
end

# the intersection of r1 and r2, the points of both
# rectangles must be relative to the same coordinate system
function intersection(r1::Space3d, r2::Rect.Rect3d)
    @assert Rect.zStart(r1) >= Rect.zStart(r2) "always intersect from below"
    rs = r1.pos[1]
    re = r1.pos[1] + r1.x
    os = r2.pos[1]
    oe = r2.pos[1] + r2.x
    if os <= rs && oe > rs
        intx = rs
        x = min(oe - rs, r1.x)
    elseif os > rs && os < re
        intx = os
        x = min(oe - os, re - os)
    else
        return None
    end

    rs = r1.pos[2]
    re = r1.pos[2] + r1.y
    os = r2.pos[2]
    oe = r2.pos[2] + r2.y
    if os <= rs && oe > rs
        inty = rs
        y = min(oe - rs, r1.y)
    elseif os > rs && os < re
        inty = os
        y = min(oe - os, re - os)
    else
        return None
    end
        
    rs = r1.pos[3]
    os = r2.pos[3]
    oe = r2.pos[3] + r2.z
    if os <= rs && oe > rs
        intz = rs
        z = oe - rs
    elseif os > rs && os < re
        @assert False "algo should not do this"
    else
        return None
    end
    Box.Box3d(0, (intx, inty, intz), x, y, z)
end    

end
