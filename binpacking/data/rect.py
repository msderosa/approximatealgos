import data.rect2d as R2

class Rect:

    def __init__(self, pos, x, y, z):
        if pos: assert len(pos) == 3, "3d positions only"
        self.pos = pos
        self.x = x
        self.y = y
        self.z = z
        self.dims = [x, y, z]
        self.dims.sort()

    def __str__(self):
        return "@{}, {} x {} x {}".format(self.pos, self.x, self.y, self.z)

    def x_start(self):
        return self.pos[0]

    def x_end(self):
        return self.pos[0] + self.x

    def y_start(self):
        return self.pos[1]

    def y_end(self):
        return self.pos[1] + self.y

    def z_start(self):
        return self.pos[2]

    def z_end(self):
        return self.pos[2] + self.z

    def area_z(self):
        return self.x * self.y

    def volume(self):
        return self.x * self.y * self.z

    def rotate90(self):
        self.x, self.y = self.y, self.x

    def floor(self):
        if self.pos:
            orgn = (self.pos[0], self.pos[1])
            return R2.Rect2d(orgn, self.x, self.y)
        else:
            return R2.Rect2d(None, self.x, self.y)
