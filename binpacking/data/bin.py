
import data.space as S
import comp_geo.rectangles as Rs
import data.rect

class Bin(data.rect.Rect):

    def __init__(self, pos, x, y, z):
        data.rect.Rect.__init__(self, pos, x, y, z)
        self.boxes = []
        self.spaces = [S.Space( (0,0,0), x, y, z)]

    def get_contents_at(self, z_pos):
        if len(self.boxes) == 0:
            return None
        else:
            raise Exception("not implemented")

    def get_box_vertices(self):
        return None

    # the minimum position at which a box could be placed
    # if it could fit - 0 is the bottom of the bin
    def fill_level(self):
        return self.spaces[0].pos[2]

    # a integer score from -1 to 1 where -1 indicates that
    # that a box can not be added to the bin and a number in
    # the range [0..] indicates the level at which the box
    # can be placed
    def affinity(self, box):
        for space in self.spaces:
            ok, dist = space.accomodates(box)
            if ok:
                return space.z_start()
        return -1

    # adds a box to the bin, must test for acceptance
    # before calling this method
    def add(self, box):
        for space in self.spaces:
            ok, dist = space.accomodates(box)
            if ok:
                b = Rs.orient_box_to(space, box)
                b.pos = space.pos
                self.boxes.append(b)
                break
        assert b, "b is not set, did you forget to call affinity?"
        ls = []
        for space in self.spaces:
            ls = ls + space.subtract(b)
        self.spaces = ls
        self.spaces.sort(key=lambda x: x.pos[2])

