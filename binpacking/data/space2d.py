
import comp_geo.points2d as Pnt
import comp_geo.rectangles2d as Rs
import data.box as B
import data.rect2d as R

class Space2d(R.Rect2d):

    def __init__(self, pos, x, y):
        R.Rect2d.__init__(self, pos, x, y)
        
    # accomodates :: Bool
    def accomodates(self, rect2d):
        return rect2d.x <= self.x and \
            rect2d.y <= self.y

    def rotationally_accomodates_as(self, box):
        b = None
        if box.dims[0] <= self.x and box.dims[1] <= self.y:
            b = B.Box(box.order, box.dims[0], box.dims[1], box.dims[2])
        elif box.dims[0] <= self.y and box.dims[1] <= self.x:
            b = B.Box(box.order, box.dims[1], box.dims[0], box.dims[2])
        return (b, self.pos)

    # subtract :: Space -> Rect2d -> [Space]
    def subtract(self, rect2d):
        r = Rs.intersection(self, rect2d)
        temp = []
        if r:
            if r.x_start() > self.x_start() + 1:
                temp.append(
                    Space2d(self.pos, 
                          r.x_start() - self.x_start(), 
                          self.y))
                assert temp[-1].area() < self.area(), "area 1 error"
            if r.x_end() + 1 < self.x_end():
                temp.append(
                    Space2d(Pnt.translate(self.pos, r.x_end() - self.x_start(), 0),
                          self.x_end() - r.x_end(),
                          self.y))
                assert temp[-1].area() < self.area(), "area 2 error"
            if r.y_start() > self.y_start() + 1:
                temp.append(
                    Space2d(self.pos,
                          self.x,
                          r.y_start() - self.y_start()))
                assert temp[-1].area() < self.area(), "area 3 error"
            if r.y_end() + 1 < self.y_end():
                temp.append(
                    Space2d(Pnt.translate(self.pos, 0, r.y_end() - self.y_start()),
                          self.x,
                          self.y_end() - r.y_end()))
                assert temp[-1].area() < self.area(), "area 4 error"
        else:
            temp.append(self)
        return temp
