
class Rect2d:

    def __init__(self, pos, x, y):
        if pos: assert len(pos) == 2, "2d positions only"
        self.pos = pos
        self.x = x
        self.y = y

    def __str__(self):
        return "@{}, {} x {}".format(self.pos, self.x, self.y)

    def __eq__(self, other):
        return self.pos == other.pos and self.x == other.x and self.y == other.y

    def __hash__(self):
        return hash(self.pos) + self.x * 71 * 71 + self.y * 71

    def x_start(self):
        return self.pos[0]

    def x_end(self):
        return self.pos[0] + self.x

    def y_start(self):
        return self.pos[1]

    def y_end(self):
        return self.pos[1] + self.y

    def area(self):
        return self.x * self.y

    def rotate90(self):
        return Rect2d(self.pos, self.y, self.x)

    def get_dims(self):
        ls = [self.x, self.y]
        ls.sort()
        return ls

