
import comp_geo.rectangles as Rs
import data.rect
import data.bin as B
import data.rect2d as R2

class Column(data.rect.Rect):

    def __init__(self, default_x=0, default_y=0):
        data.rect.Rect.__init__(self, (0,0,0), default_x, default_y, 0)
        self.boxes = []
        self.cursor = (0, 0) #z_pos, boxes_idx
        self.pos = None
        self.col_idx = None
  
    def add(self, box):
        box.pos = (0, 0, self.z)
        self.boxes.append(box)
        if box.x > self.x: 
            self.x = box.x
        if box.y > self.y:
            self.y = box.y
        self.z = self.z + box.z

    def add_at_z(self, box, z_pos):
        assert box.x <= self.x and box.y <= self.y, "this fn never changes x,y dims"
        dz = z_pos - self.z
        assert dz >= 0, "added below the top box"
        if dz > 0:
            bin = B.Bin((0, 0, self.z), self.x, self.y, dz)
            self.boxes.append(bin)
            self.z = self.z + dz
        self.add(box)

    def floor(self):
        if self.pos:
            orgn = (self.pos[0], self.pos[1])
            return R2.Rect2d(orgn, self.x, self.y)
        else:
            return R2.Rect2d(None, self.x, self.y)

    def rotate90(self):
        self.x, self.y = self.y, self.x
        for b in self.boxes:
            b.x, b.y = b.y, b.x

    def sort(self):
        self.boxes.sort(key=lambda b: b.order)
        z_height = 0
        for b in self.boxes:
            b.pos = (0, 0, z_height)
            z_height = z_height + b.z

    def get_box_vertices(self, z_offset):
        assert len(self.boxes) > 0, "expecting boxes"
        assert self.pos != None, "column should have a position"
        assert self.pos[0] < 1000 and self.pos[1] < 1000 and self.pos[2] == 0
        recs = [b.get_box_vertices() for b in self.boxes]
        ls = []
        for rec in recs:
            if rec == None:
                continue
            offset_rec = []
            offset_rec.append(rec[0])
            for pnt in rec[1:]:
                temp = (pnt[0] + self.pos[0], pnt[1] + self.pos[1], z_offset + 1 - pnt[2])
                offset_rec.append(temp)
            ls.append(offset_rec)
        return ls
