
module Rect

abstract Rect3d

print(r::Rect3d) = "$(r.pos), $(r.x) x $(r.y) x $(r.z)"

xStart(r::Rect3d) = r.pos[1]

xEnd(r::Rect3d) = r.pos[1] + r.x

yStart(r::Rect3d) = r.pos[2]

yEnd(r::Rect3d) = r.pos[2] + r.y

zStart(r::Rect3d) = r.pos[3]

zEnd(r::Rect3d) = r.pos[3] + r.z

zArea(r::Rect3d) = r.x * r.y

volume(r::Rect3d) = r.x * r.y * r.z

function intersect(ra::Rect3d, rb::Rect3d)
    
end

end
