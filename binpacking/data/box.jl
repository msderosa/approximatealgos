require("data/rect.jl")

module Box
using Rect

export mkBox, Box3d

type Box3d <: Rect.Rect3d
    order::Int
    pos
    x::Int
    y::Int
    z::Int
end

function mkBox(order, x, y, z, fnOrient)
    b = Box3d(order, None, x, y, z)
    fnOrient(b)
    b
end

end

## class Box(data.rect.Rect):

##     def __init__(self, order, x, y, z, orientation=None):
##         data.rect.Rect.__init__(self, None, x, y, z)
##         if type(order) == list:
##             self.order = order
##         else:
##             self.order = [order]
##         self.boxes = []
##         if orientation:
##             orientation(self)

##     def get_contents_at(self, z_pos):
##         if z_pos == self.pos[2]:
##             return self
##         else:
##             return None

##     def _add(self, box, pos):
##         self.boxes.append((pos, box))
##         return self

##     pack_xy :: Box -> Box -> Box
##     def pack_x(self, other):
##         assert other.z == self.z, "for x y packing we want the heights to be the same"
##         if self.y == other.y:
##             b = Box(self.order + other.order, self.x + other.x, self.y, self.z)
##             b._add(self, (0, 0, 0))
##             b._add(b, (0, self.y, 0))
##         elif self.y == other.x:
##             rot90 = Box(other.order, other.y, other.x, other.z)
##             b = Box(self.order + other.order, self.x + rot90.x, self.y, self.z)
##             b._add(self, (0, 0, 0))
##             b._add(rot90, (0, self.y, 0))            
##         else:
##             raise Exception("cant pack non matching boxes along x")
##         return b

##     def pack_y(self, other):
##         assert other.z == self.z, "for x y packing we want the heights to be the same"
##         if self.x == other.x:
##             b = Box(self.order + other.order, self.x, self.y + other.y, self.z)
##             b._add(self, (0, 0, 0))
##             b._add(b, (self.x, 0, 0))
##         elif self.x == other.y:
##             rot90 = Box(other.order, other.y, other.x, other.z)
##             b = Box(self.order + other.order, self.x, self.y + rot90.y, self.z)
##             b._add(self, (0, 0, 0))
##             b._add(rot90, (0, self.y, 0))            
##         else:
##             raise Exception("cant pack non matching boxes along y")
##         return b

##     boxes translated into contest frame of ref
##     def get_box_vertices(self):
##         if len(self.order) == 1:
##             orgn = (self.pos[0] + 1, self.pos[1] + 1, self.pos[2] + 1)
##             return [self.order[0],
##                     (orgn[0],              orgn[1],              orgn[2]),
##                     (orgn[0] + self.x - 1, orgn[1],              orgn[2]),
##                     (orgn[0] + self.x - 1, orgn[1] + self.y - 1, orgn[2]),
##                     (orgn[0],              orgn[1] + self.y - 1, orgn[2]),
##                     (orgn[0],              orgn[1],              orgn[2] + self.z - 1),
##                     (orgn[0] + self.x - 1, orgn[1],              orgn[2] + self.z - 1),
##                     (orgn[0] + self.x - 1, orgn[1] + self.y - 1, orgn[2] + self.z - 1),
##                     (orgn[0],              orgn[1] + self.y - 1, orgn[2] + self.z - 1)]
##         else:
##             raise Exception("not impl")

##     def __eq__(self, other):
##         return self.order == other.order

##     def __hash__(self):
##         hs = 0
##         for i, n in enumerate(self.order):
##             hs = hs + n * math.pow(17, i)
##         return int(hs)
