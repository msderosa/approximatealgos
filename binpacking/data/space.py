
import comp_geo.points as Pnt
import comp_geo.rectangles as Rs
import data.rect

class Space(data.rect.Rect):

    def __init__(self, pos, x, y, z):
        data.rect.Rect.__init__(self, pos, x, y, z)
        
    # accomodates :: (Bool, Integer)
    # retuns an indication of the ability of the space to hold
    # the box and a manhattan distance meausre of the goodness of fit
    def accomodates(self, box):
        fits = box.dims[0] <= self.dims[0] and \
            box.dims[1] <= self.dims[1] and \
            box.dims[2] <= self.dims[2]

        mhtn_dist = (self.x - box.x) + (self.y - box.y) + (self.z - box.z)
        return (fits, mhtn_dist)

    # subtract :: Space -> Box -> [Space]
    def subtract(self, box):
        r = Rs.intersection(self, box)
        temp = []
        if r:
            if r.x_start() > self.x_start() + 1:
                temp.append(
                    Space(self.pos, 
                          r.x_start() - self.x_start(), 
                          self.y, self.z))
            if r.x_end() + 1 < self.x_end():
                temp.append(
                    Space(Pnt.translate(self.pos, r.x_end() - self.x_start(), 0, 0),
                          self.x_end() - r.x_end(),
                          self.y, self.z))

            if r.y_start() > self.y_start() + 1:
                temp.append(
                    Space(self.pos,
                          r.x,
                          r.y_start() - self.y_start(), 
                          self.z))
            if r.y_end() + 1 < self.y_end():
                temp.append(
                    Space(Pnt.translate(self.pos, 0, r.y_end() - self.y_start(), 0),
                          self.x,
                          self.y_end() - r.y_end(),
                          self.z))

            if r.z_start() > self.z_start() + 1:
                temp.append(
                    Space(self.pos,
                          self.x, self.y,
                          r.z_start() - self.z_start()))
            if r.z_end() + 1 < self.z_end():
                temp.append(
                    Space(Pnt.translate(self.pos, 0, 0, r.z_end() - self.z_start()),
                          self.y, self.z,
                          self.z_end() - r.z_end()))
        else:
            temp.append(self)
        return temp
            
