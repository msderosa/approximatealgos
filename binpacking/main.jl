require("parser.jl")
require("container.jl")

using Container

println("starting...")
bs = Parser.parse()

sleigh = Container.mkSleigh()
Container.addBoxes(sleigh, bs)
Container.output(sleigh)
    
println("success...")




