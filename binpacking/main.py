
import parser
import algos.partitioned_list as Pl
import container as C
import pdb

def main():
    print("starting...")
    bs = parser.parse()

    container = C.Container(1000)
    container.add_boxes(bs)

    print("output...")
    container.output()
    print("success...")

def parse_reorient():
    print("parsing begin...")
    bs = parser.parse()
    return [b.rotate_random() for b in bs]

def process(ls, stats):
    print("processing begin...")
    bs = XyP.pack(ls)
    return bs

if __file__ == 'main.py':
    main()


