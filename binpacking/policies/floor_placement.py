
import comp_geo.rectangles2d as Comp
import comp_geo.points2d as Pnt

def position_btm_left(container, space, col_floor):
    floor, is_rotated = _orient_best_manh_fit(space, col_floor)
    return (space.pos, is_rotated)

def position_btm_left_worstfit(container, space, col_floor):
    floor, is_rotated = _orient_worst_fit(space, col_floor)
    return (space.pos, is_rotated)    

def position_favoring_edges(container, space, col_floor):
    floor, is_rotated = _orient_best_abs_fit(space, col_floor)
    if container.pos[0] == space.pos[0]:
        flr_x = space.pos[0]
    elif container.pos[0] + container.dim == space.pos[0] + space.x:
        flr_x = container.pos[0] + container.dim - floor.x
    else:
        flr_x = space.pos[0]

    if container.pos[1] == space.pos[1]:
        flr_y = space.pos[1]
    elif container.pos[1] + container.dim == space.pos[1] + space.y:
        flr_y = container.pos[1] + container.dim - floor.y
    else:
        flr_y = space.pos[1]
    return ( (flr_x, flr_y), is_rotated)

def position_favoring_edges2(container, space, col_floor):
    floor, is_rotated = _orient_best_abs_fit(space, col_floor)
    dx = floor.x / 2.0
    dy = floor.y / 2.0

    pnt_tl = Pnt.translate(space.pos, 0, space.y)
    flr_center_tl = Pnt.translate(pnt_tl, dx, -1 * dy)
    d_tl = Pnt.distance((500, 500), flr_center_tl)

    pnt_tr = Pnt.translate(space.pos, space.x, space.y)
    flr_center_tr = Pnt.translate(pnt_tr, -1 * dx, -1 * dy)
    d_tr = Pnt.distance((500, 500), flr_center_tr)

    pnt_bl = space.pos
    flr_center_bl = Pnt.translate(pnt_bl, dx, dy)
    d_bl = Pnt.distance((500, 500), flr_center_bl)

    pnt_br = Pnt.translate(space.pos, space.x, 0)
    flr_center_br = Pnt.translate(pnt_br, -1 * dx, dy)
    d_br = Pnt.distance((500, 500), flr_center_br)

    max_d = max(d_tl, d_tr, d_bl, d_br)
    if max_d == d_tl:
        pos = Pnt.translate(pnt_tl, 0, -1 * floor.y)
    elif max_d == d_tr:
        pos = Pnt.translate(pnt_tr, -1 * floor.x, -1 * floor.y)
    elif max_d == d_bl:
        pos = pnt_bl
    elif max_d == d_br:
        pos = Pnt.translate(pnt_br, -1 * floor.x, 0)
    else:
        raise Exception("choice error")

    return (pos, is_rotated)

def position_favoring_border(container, space, col_floor):
    pass

def _orient_best_abs_fit(space, floor0):
    floor90 = floor0.rotate90()
    dx0, dy0 = space.x - floor0.x, space.y - floor0.y
    dx90, dy90 = space.x - floor90.x, space.y - floor90.y
    if dx0 < 0 or dy0 < 0:
        return (floor90, True)
    if dx90 < 0 or dy90 < 0:
        return (floor0, False)
    if min(dx0, dy0) <= min(dx90, dy90):
        return (floor0, False)
    else:
        return (floor90, True)
    
def _orient_best_manh_fit(space, col_floor):
    floor90 = col_floor.rotate90()
    if col_floor.x == space.x and space.y >= col_floor.y:
        return (col_floor, False)
    elif col_floor.x == space.y and space.x >= col_floor.y:
        return (floor90, True)
    elif col_floor.y == space.y and space.x >= col_floor.x:
        return (col_floor, False)
    elif col_floor.y == space.x and space.y >= col_floor.x:
        return (floor90, True)

    dist0 = Comp.manhattan_dist(space, col_floor)
    dist90 = Comp.manhattan_dist(space, floor90)
    if dist0 <= dist90:
        return (col_floor, False)
    else:
        return (floor90, True)

def _orient_best_eucl_fit(space, col_floor):
    floor90 = col_floor.rotate90()
    if col_floor.x == space.x and space.y >= col_floor.y:
        return (col_floor, False)
    elif col_floor.x == space.y and space.x >= col_floor.y:
        return (floor90, True)
    elif col_floor.y == space.y and space.x >= col_floor.x:
        return (col_floor, False)
    elif col_floor.y == space.x and space.y >= col_floor.x:
        return (floor90, True)

    dist0 = Comp.euclidian_dist(space, col_floor)
    dist90 = Comp.euclidian_dist(space, floor90)
    if dist0 >= dist90:
        return (col_floor, False)
    else:
        return (floor90, True)

def _orient_worst_fit(space, col_floor):
    floor90 = col_floor.rotate90()
    dist0 = Comp.manhattan_dist(space, col_floor)
    dist90 = Comp.manhattan_dist(space, floor90)
    inf = float("inf")
    if dist0 == inf:
        return (floor90, True)
    elif dist90 == inf:
        return (col_floor, False)
    elif dist0 <= dist90:
        return (floor90, True)
    else:
        return (col_floor, False)
