
import math

def min_area_space_first(spaces):
    return sorted(spaces, key=lambda x: x.area())

def max_area_space_first(spaces):
    return sorted(spaces, key=lambda x: x.area(), reverse=True)

def from_bottom_up(spaces):
    return sorted(spaces, key=lambda x: x.pos[1])

def from_bottom_left(spaces):
    return sorted(spaces, key=lambda x: x.pos[0] * x.pos[1])

def by_min_dimension(spaces):
    return sorted(spaces, key=lambda x: min(x.x, x.y))

# not quite right as we want to use the space center not origin
# and not dimensions like below
def distance_from_center(spaces):
    return sorted(spaces, key=lambda s: pow(abs(500 -s.x), 2) + pow(abs(500 - s.y), 2))
