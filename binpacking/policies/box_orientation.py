
import random

def randomD(box):
    l, w, h = box.x, box.y, box.z
    switch_l_w = random.choice([0,1])
    if switch_l_w:
        w, l = l, w
    switch_l_h = random.choice([0,1])
    if switch_l_h:
        l, h = h, l
    switch_w_h = random.choice([0,1])
    if switch_w_h:
        w, h = h, w
    box.x = l
    box.y = w
    box.z = h

def x_decreasingD(box):
    ls = [box.x, box.y, box.z]
    ls.sort(reverse=True)
    box.x = ls[0]
    box.y = ls[1]
    box.z = ls[2]

    
