require("data/box.jl")

module Policy
module Orientation

using Box

function xLargeZSmall!(box::Box.Box3d)
    ls = [box.x, box.y, box.z]
    sort(ls)
    box.x = ls[3]
    box.y = ls[2]
    box.z = ls[1]
end

end    
end
