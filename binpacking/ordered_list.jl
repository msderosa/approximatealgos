require("data/space.jl")

module Spaces
using Space

export mkSpaceSet, substract

type SpaceSet
    cursor::Int
    size::Int
    spaces_at
end

function push!(ss::SpaceSet, rs::Array{Space.Space3d, 1})
    for rem in rs
        ss.size = ss.size + 1
    end
end
    
function mkSpaceSet(dim::Int)
    SpaceSet(0,
             1,
             [0 => Space.Space3d((0,0,0), dim, dim)])
end

end
