
import comp_geo.hilbert_curve as H

class HilbertSort:

    def __init__(self, xrange, yrange):
        self.xmin, self.xmax = xrange
        self.dcol = self.xmax - self.xmin + 1

        self.ymin, self.ymax = yrange
        self.drow = self.ymax - self.ymin + 1

        self.curve = H.HilbertCurve(max(
                self.dcol, 
                self.drow))

    def sort(self, box):
        hx = box.x - self.xmin
        hy = box.y - self.ymin
        return [self.curve.get_step(hx, hy), box.order]

    def acceptsQ(self, box):
        return box.x >= self.xmin and \
            box.x <= self.xmax and \
            box.y >= self.ymin and \
            box.y <= self.ymax
    
