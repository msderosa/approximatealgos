
import math 

def translate(pnt, x_off, y_off):
    return (pnt[0] + x_off,
            pnt[1] + y_off)

def distance(pnt1, pnt2):
    return math.sqrt(
        math.pow(pnt1[0] - pnt2[0], 2) +
        math.pow(pnt1[1] - pnt2[1], 2))
