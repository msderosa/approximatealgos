
import data.rect2d as R
import math

def manhattan_dist(r1, r2):
    if r1.x >= r2.x and r1.y >= r2.y:
        return (r1.x - r2.x) + (r1.y - r2.y)
    else:
        return float("inf")

def euclidian_dist(r1, r2):
    if r1.x >= r2.x and r1.y >= r2.y:
        return pow(r1.x - r2.x, 2) + pow(r1.y - r2.y, 2)
    else:
        return -1

# retuns an indication of the ability of the one space to hold
# another
def accomodates(r1, r2):
    return r1.dims[0] >= r2.dims[0] and \
        r1.dims[1] >= r2.dims[1]

def intersect(r1, r2):
    t = r1.pos[0] - r2.pos[0]
    if t > r2.x or -t > r1.x:
        return False
    t = r1.pos[1] - r2.pos[1]
    if t > r2.y or -t > r1.y:
        return False
    return True

def intersection(r1, r2):
    t = r1.pos[0] - r2.pos[0]
    if t >= r2.x or -t >= r1.x:
        return None
    t = r1.pos[1] - r2.pos[1]
    if t >= r2.y or -t >= r1.y:
        return None
    
    rs = r1.pos[0]
    re = r1.pos[0] + r1.x
    os = r2.pos[0]
    oe = r2.pos[0] + r2.x
    if os <= rs and oe > rs:
        intx = rs
        x = min(oe - rs, r1.x)
    elif os > rs and os < re:
        intx = os
        x = min(oe - os, re - os)
    else:
        assert False, "error in x interection"

    rs = r1.pos[1]
    re = r1.pos[1] + r1.y
    os = r2.pos[1]
    oe = r2.pos[1] + r2.y
    if os <= rs and oe > rs:
        inty = rs
        y = min(oe - rs, r1.y)
    elif os > rs and os < re:
        inty = os
        y = min(oe - os, re - os)
    else:
        assert False, "error in y intersection"

    rslt = R.Rect2d((intx, inty), x, y)
    assert rslt.area() <= r1.area(), "bad area relative to r1"
    assert rslt.area() <= r2.area(), "bad area relative to r2"
    return rslt
    

        
