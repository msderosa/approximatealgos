
def translate(pnt, x_off, y_off, z_off):
    return (pnt[0] + x_off,
            pnt[1] + y_off,
            pnt[2] + z_off)
