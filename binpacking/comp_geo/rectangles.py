
import data.rect as R
import data.box as B

# orient other so that it has the proper orientation to be
# added to fixed
def orient_box_to(fixed, other):
    assert other.pos == None, 'should not have a position yet'
    x = y = z = None
    for i, dim in enumerate(fixed.dims):
        if not x and fixed.dims[i] == fixed.x:
            x = other.dims[i]
        elif not y and fixed.dims[i] == fixed.y:
            y = other.dims[i]
        elif not z and fixed.dims[i] == fixed.z:
            z = other.dims[i]
        else:
            raise Exception('orientation error')
    return B.Box(other.order, x, y, z)

# the intersection of r1 and r2, the points of both
# rectangles must be relative to the same coordinate system
def intersection(r1, r2):
    rs = r1.pos[0]
    re = r1.pos[0] + r1.x
    os = r2.pos[0]
    oe = r2.pos[0] + r2.x
    if os <= rs and oe > rs:
        intx = rs
        x = min(oe - rs, r1.x)
    elif os > rs and os < re:
        intx = os
        x = min(oe - os, re - os)
    else:
        return None

    rs = r1.pos[1]
    re = r1.pos[1] + r1.y
    os = r2.pos[1]
    oe = r2.pos[1] + r2.y
    if os <= rs and oe > rs:
        inty = rs
        y = min(oe - rs, r1.y)
    elif os > rs and os < re:
        inty = os
        y = min(oe - os, re - os)
    else:
        return None

    rs = r1.pos[2]
    re = r1.pos[2] + r1.z
    os = r2.pos[2]
    oe = r2.pos[2] + r2.z
    if os <= rs and oe > rs:
        intz = rs
        z = min(oe - rs, r1.z)
    elif os > rs and os < re:
        intz = os
        z = min(oe - os, re - os)
    else:
        return None

    return R.Rect((intx, inty, intz), x, y, z)
    

        
