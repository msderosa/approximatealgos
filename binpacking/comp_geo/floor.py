
from numpy import *

class Floor:

    def __init__(self, dim):
        self.grid = zeros((dim, dim), dtype=int8)
        self.insertion_pnts = [(0,0)]
        self.free_space = dim * dim
        self.zlevel = 0

    def get_dimension(self):
        return self.grid.shape[0]

    def increment_zlevel():
        self.zlevel = self.zlevel + 1

    def free_area(self, rect):
        self.free_space = self.free_space + rect.area()
        self._free_grid_area(rect)
        self._free_area_insertion_pnts(rect)

    def _free_area_insertion_pnts(self, rect):
        br = (rect.x_end(), rect.y_start())
        tl = (rect.x_start(), rect.y_end())
        tl = self._translate_left(tl)
        self.insertion_pnts.remove(br)
        self.insertion_pnts.remove(tl)
        self.insertion_pnts.append(rect.pos)
        self.insertion_pnts.sort(key=lambda p: p[1])

    def _free_grid_area(self, rect):
        x, y = rect.pos
        for col in range(x, x + rect.x):
            for row in range(y, y + rect.y):
                self.grid[row, col] = 0       

    def place(self, box):
        temp = None
        for pos in self.insertion_pnts:
            if self.accepts(box, pos):
                self.place_at(box, pos)
                temp = pos
                break
        return temp

    def place_at(self, box, pos):
        x, y = pos
        box.pos = (x, y, self.zlevel)

        self._insert_box_insertion_pnts(box, pos)
        self._insert_grid_area(box)
        self.free_space = self.free_space - box.area_z()

    def _insert_grid_area(self, box):
        x, y, z = box.pos
        for col in range(x, x + box.x):
            for row in range(y, y + box.y):
                self.grid[row, col] = 1

    def _insert_box_insertion_pnts(self, box, pos):
        br = (box.x_end(), box.y_start())
        tl = (box.x_start(), box.y_end())
        tl = self._translate_left(tl)
        self.insertion_pnts.remove(pos)
        self.insertion_pnts.append(br)
        self.insertion_pnts.append(tl)
        self.insertion_pnts.sort(key=lambda p: p[1])

    def _translate_left(self, pnt):
        best_x, y = pnt
        try_x = best_x - 1
        while self.grid[try_x, y] == 0 and try_x >= 0:
            best_x = try_x
            try_x = try_x - 1
        return (best_x, y)
        
    def accepts(self, box, pos):
        dim = self.get_dimension()
        x, y = pos
        if x + box.x > dim or y + box.y > dim:
            return False
        else:
            col = x
            row = y
            bottom_slice = self.grid[row, col:col + box.x + 1]
            if any(bottom_slice):
                return False
            left_slice = self.grid[row:row + box.y + 1, col]
            if any(left_slice):
                return False
            return True
