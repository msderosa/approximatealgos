
from numpy import *

class HilbertCurve:

    def __init__(self, target_size):
        self.target_size = target_size
        self.grid = self._build_grid(
            zeros((1, 1), dtype=int16), target_size)
        

    def _build_grid(self, cg, target_size):
        if cg.shape[0] < target_size:
            rot_ll = self._reverse_numbering(self._rotate_p90(cg))
            rot_lr = self._reverse_numbering(self._rotate_n90(cg))
            bigger = self._grow(cg, rot_ll, rot_lr)
            return self._build_grid(bigger, target_size)
        else:
            return cg

    def _rotate_p90(self, grid):
        dim = grid.shape[0]
        temp = zeros(grid.shape, dtype=int16)
        for col in list(range(dim - 1, -1, -1)):
            for row in list(range(dim - 1, -1, -1)):
                temp[col, dim - 1 - row] = grid[row, col]
        return temp

    def _rotate_n90(self, grid):
        dim = grid.shape[0]
        temp = zeros(grid.shape, dtype=int16)
        for col in list(range(dim - 1, -1, -1)):
            for row in list(range(dim)):
                temp[dim - 1 - col, row] = grid[row, col]
        return temp
    
    def _reverse_numbering(self, rot):
        dim = rot.shape[0]
        sub = [dim * dim - 1] * (dim * dim)
        rev = (rot - array(sub).reshape(dim, dim)) * -1
        return rev

    def _grow(self, grid, ll, lr):
        dim = grid.shape[0]
        tl = grid + ones(grid.shape, dtype=int16) * (dim * dim * 1)
        tr = grid + ones(grid.shape, dtype=int16) * (dim * dim * 2)
        top = hstack( (tl, tr) )
        lr = lr + ones(grid.shape, dtype=int16) * (dim * dim * 3)
        bottom = hstack( (ll, lr) )
        return vstack( (top, bottom) )
        
    def get_step(self, x, y):
        dim = self.grid.shape[0]
        if x < self.target_size and y < self.target_size:
            return self.grid[dim - 1 - x, y]
        else:
            return None
