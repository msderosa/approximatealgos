
module Points

export translate

function translate(pnt::(Int, Int, Int), xOff::Int, yOff::Int, zOff::Int)
    (pnt[1] + xOff, pnt[2] + yOff, pnt[3] + zOff)
end

end
